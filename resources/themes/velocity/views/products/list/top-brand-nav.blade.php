@inject ('productRepository', 'Webkul\Product\Repositories\ProductRepository')
@inject ('attributeRepository', 'Webkul\Attribute\Repositories\AttributeRepository')
@inject ('productFlatRepository', 'Webkul\Product\Repositories\ProductFlatRepository')

<?php
$filterAttributes = $attributes = [];
$maxPrice = 0;

if (isset($category)) {
    $filterAttributes = $productFlatRepository->getProductsRelatedFilterableAttributes($category);

    $maxPrice = core()->convertPrice($productFlatRepository->getCategoryProductMaximumPrice($category));
}

if (!count($filterAttributes) > 0) {
    $filterAttributes = $attributeRepository->getFilterAttributes();
}
//dd($filterAttributes);
?>

@if (count($filterAttributes) > 0)
    @foreach($filterAttributes as $filterAttribute)
        @if(!\Illuminate\Support\Facades\Request::input($filterAttribute->code))
            @if($filterAttribute->code == 'MobileBrands')
                <div class="row remove-padding-margin">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding text-center">
                        <div class="list-group list-group-horizontal">
                            @if(!empty($filterAttribute['options']))
                                @php $count =0; @endphp
                                @foreach($filterAttribute['options'] as $option)
                                    @if($count<6)
                                        <a href="{{\Illuminate\Support\Facades\Request::url()}}?{{!empty($filterAttribute->code)?$filterAttribute->code.'='.$option['id']:''}}"
                                           class="list-group-item " style="   max-width: 20%;">
                                            <img class="c36f5Z"
                                                 src="{{asset('vendor/webkul/ui/assets/images/brands').'/'.str_replace(" ",'-',strtolower($option['admin_name'])).'.jpg'}}">
                                        </a>
                                    @endif
                                    @php $count++; @endphp
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        @endif
    @endforeach
@endif
