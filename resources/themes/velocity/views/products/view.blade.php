@extends('shop::layouts.master')

@inject ('reviewHelper', 'Webkul\Product\Helpers\Review')
@inject ('customHelper', 'Webkul\Velocity\Helpers\Helper')

@php
    $total = $reviewHelper->getTotalReviews($product);

    $avgRatings = $reviewHelper->getAverageRating($product);
    $avgStarRating = round($avgRatings);

    $productImages = [];
    $images = productimage()->getGalleryImages($product);

    foreach ($images as $key => $image) {
        array_push($productImages, $image['medium_image_url']);
    }
@endphp

@section('page_title')
    {{ trim($product->meta_title) != "" ? $product->meta_title : $product->name }}
@stop

@section('seo')
    <meta name="description"
          content="{{ trim($product->meta_description) != "" ? $product->meta_description : \Illuminate\Support\Str::limit(strip_tags($product->description), 120, '') }}"/>

    <meta name="keywords" content="{{ $product->meta_keywords }}"/>

    @if (core()->getConfigData('catalog.rich_snippets.products.enable'))
        <script type="application/ld+json">
            {!! app('Webkul\Product\Helpers\SEO')->getProductJsonLd($product) !!}
        </script>
    @endif

    <?php $productBaseImage = productimage()->getProductBaseImage($product); ?>

    <meta name="twitter:card" content="summary_large_image"/>

    <meta name="twitter:title" content="{{ $product->name }}"/>

    <meta name="twitter:description" content="{{ $product->description }}"/>

    <meta name="twitter:image:alt" content=""/>

    <meta name="twitter:image" content="{{ $productBaseImage['medium_image_url'] }}"/>

    <meta property="og:type" content="og:product"/>

    <meta property="og:title" content="{{ $product->name }}"/>

    <meta property="og:image" content="{{ $productBaseImage['medium_image_url'] }}"/>

    <meta property="og:description" content="{{ $product->description }}"/>

    <meta property="og:url" content="{{ route('shop.productOrCategory.index', $product->url_key) }}"/>
@stop

@push('css')
    <style type="text/css">
        .related-products {
            width: 100%;
        }

        .recently-viewed {
            margin-top: 20px;
        }

        .store-meta-images > .recently-viewed:first-child {
            margin-top: 0px;
        }

        .main-content-wrapper {
            margin-bottom: 0px;
        }
    </style>
@endpush

@section('full-content-wrapper')
    {!! view_render_event('bagisto.shop.products.view.before', ['product' => $product]) !!}

    <div class="row no-margin">
        <section class="col-12 product-detail">
            <div class="layouter">
                <product-view>
                    <div class="form-container">
                        @csrf()

                        <input type="hidden" name="product_id" value="{{ $product->product_id }}">

                        <div class="row  ">
                            <div data-spm="breadcrumb" id="J_breadcrumb_list"
                                 class="breadcrumb_list breadcrumb_custom_cls">
                                <ul class="breadcrumb" id="J_breadcrumb">
                                    @foreach($breadcrumb as $item)
                                        @if($item!=null && $item->name !='Root')
                                            <li class="breadcrumb_item">
                                            <span class="breadcrumb_item_text">
                                                        <a title="Home Appliances" href="{{$item->url_path}}"
                                                           class="breadcrumb_item_anchor">
                                                                <span>{{$item->name}}</span>
                                                        </a>
                                            <div class="breadcrumb_right_arrow"></div>
                                            </span>
                                            </li>
                                        @endif
                                    @endforeach
                                    <li class="breadcrumb_item">
                                            <span class="breadcrumb_item_text">
                                                        <a title="Home Appliances" class="breadcrumb_item_anchor">
                                                                <span>{{$product->name}}</span>
                                                        </a>
                                            </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row  ">
                            {{-- product-gallery --}}

                            <div class="left col-lg-4 col-md-4  no-pad pdbg">
                                @include ('shop::products.view.gallery')
                            </div>

                            {{-- right-section --}}
                            <div class="right col-lg-4 col-md-4 pdbg">
                                {{-- product-info-section --}}
                                <div class="row info">
                                    <div class="col-md-12 ">
                                        <h2 class="">{{ $product->name }}</h2>
                                        @if ($total)
                                            <div class="reviews col-lg-12">
                                                <star-ratings
                                                        push-class="mr5"
                                                        :ratings="{{ $avgStarRating }}"
                                                ></star-ratings>

                                                <div class="reviews">
                                                    <span>
                                                        {{ __('shop::app.reviews.ratingreviews', [
                                                            'rating' => $avgRatings,
                                                            'review' => $total])
                                                        }}
                                                    </span>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="row pd-15">

                                            {{--<div class="col-md-6">--}}
                                            {{--@if ($product->getTypeInstance()->showQuantityBox())--}}
                                            {{--<div>--}}
                                            {{--<quantity-changer></quantity-changer>--}}
                                            {{--</div>--}}
                                            {{--@else--}}
                                            {{--<input type="hidden" name="quantity" value="1">--}}
                                            {{--@endif--}}
                                            {{--</div>--}}

                                                @include ('shop::products.view.stock', ['product' => $product])

                                                <div class="col-6 pricepage">
                                                    @include ('shop::products.price', ['product' => $product])
                                                </div>

                                        </div>


                                        @if (count($product->getTypeInstance()->getCustomerGroupPricingOffers()) > 0)
                                            <div class="col-12">
                                                @foreach ($product->getTypeInstance()->getCustomerGroupPricingOffers() as $offers)
                                                    {{ $offers }} <br>
                                                @endforeach
                                            </div>
                                        @endif
                                        <add-to-cart-custom
                                                {{--form="true"--}}
                                                csrf-token='{{ csrf_token() }}'
                                                full-product='{{ $product}}'
                                                product-flat-id="{{ $product->id }}"
                                                product-id="{{ $product->product_id }}"
                                                reload-page="{{ false }}"
                                                move-to-cart="{{ $moveToCart ?? false }}"
                                                wishlist-move-route="{{ $wishlistMoveRoute ?? false }}"
                                                add-class-to-btn="{{ $addToCartBtnClass ?? 'custombtn' }}"
                                                is-enable={{ ! $product->isSaleable() ? 'false' : 'true' }}
                                                        show-cart-icon={{ ! (isset($showCartIcon) && ! $showCartIcon) }}
                                                        btn-text="{{ (! isset($moveToCart) && $product->type == 'booking') ?  __('shop::app.products.book-now') : $btnText ?? __('shop::app.products.add-to-cart') }}">
                                        </add-to-cart-custom>
                                        {{--<div class="product-actions">--}}
                                        {{--@if (core()->getConfigData('catalog.products.storefront.buy_now_button_display'))--}}
                                        {{--@include ('shop::products.buy-now', [--}}
                                        {{--'product' => $product,--}}
                                        {{--])--}}
                                        {{--@endif--}}

                                        {{--@include ('shop::products.add-to-cart', [--}}
                                        {{--'form' => false,--}}
                                        {{--'product' => $product,--}}
                                        {{--'showCartIcon' => false,--}}
                                        {{--'reloadPage'=>false,--}}
                                        {{--'showCompare' => core()->getConfigData('general.content.shop.compare_option') == "1"--}}
                                        {{--? true : false,--}}
                                        {{--])--}}
                                        {{--</div>--}}
                                        {!! view_render_event('bagisto.shop.products.view.short_description.before', ['product' => $product]) !!}

                                        @if ($product->short_description)
                                            <div class="description">
                                                <h3 class="col-lg-12">{{ __('velocity::app.products.short-description') }}</h3>

                                                {!! $product->short_description !!}
                                            </div>
                                        @endif
                                        {!! view_render_event('bagisto.shop.products.view.short_description.after', ['product' => $product]) !!}


                                        {!! view_render_event('bagisto.shop.products.view.quantity.before', ['product' => $product]) !!}



                                        {!! view_render_event('bagisto.shop.products.view.quantity.after', ['product' => $product]) !!}

                                    </div>


                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <delivery></delivery>
                                </div>
                            </div>

                        </div>
                        <div class="row al1 mt6">

                            <div class="col-md-12">
                                <div class="card ">
                                    <div class="card-body ">
                                        @include ('shop::products.view.configurable-options')

                                        @include ('shop::products.view.downloadable')

                                        @include ('shop::products.view.grouped-products')

                                        @include ('shop::products.view.bundle-options')

                                        @include ('shop::products.view.attributes', [
                                            'active' => true
                                        ])

                                        {{-- product long description --}}
                                        @include ('shop::products.view.description')
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row al1 mt6">

                            <div class="col-md-12">
                                <div class="card ">
                                    <div class="card-body ">
                                        {{-- reviews count --}}
                                        @include ('shop::products.view.reviews', ['accordian' => true])
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </product-view>

            </div>
        </section>

        <div class="related-products">

            @include('shop::products.view.related-products')
            @include('shop::products.view.up-sells')
        </div>

        <div class="store-meta-images col-3">
            @if(
                isset($velocityMetaData['product_view_images'])
                && $velocityMetaData['product_view_images']
            )
                @foreach (json_decode($velocityMetaData['product_view_images'], true) as $image)
                    @if ($image && $image !== '')
                        <img src="{{ url()->to('/') }}/storage/{{ $image }}" alt=""/>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
    @inject ('productViewHelper', 'Webkul\Product\Helpers\View')
    @php
        $money_back_guarantee = 'No';
        $warranty = 'No Warranty';
            $customAttributeValues = $productViewHelper->getAdditionalData($product);
            if($customAttributeValues){
                foreach ($customAttributeValues as $attribute){
                    if($attribute['code'] === 'Warranty'){
                        $warranty = $attribute['value'];
                    }
                    if($attribute['code'] === 'MoneyBackGarrentee'){
                        $money_back_guarantee = $attribute['value'];
                    }
                }
            }
    @endphp
    {!! view_render_event('bagisto.shop.products.view.after', ['product' => $product]) !!}
@endsection

@push('scripts')

    <script type='text/javascript' src='https://unpkg.com/spritespin@4.1.0/release/spritespin.js'></script>

    <script type="text/x-template" id="product-view-template">
        <form
                method="POST"
                id="product-form"
                @click="onSubmit($event)"
                action="{{ route('cart.add', $product->product_id) }}">

            <input type="hidden" name="is_buy_now" v-model="is_buy_now">

            <slot v-if="slot"></slot>

            <div v-else>
                <div class="spritespin"></div>
            </div>

        </form>
    </script>

    <script type="text/x-template" id="delivery-template">
        <div class="card-body delivery">
            <div class="row dr">
                <div class="col-md-12">
                    <h6 class="phead">Delivery Option</h6>
                </div>
                <div class="col-2 mt6"><span class="lnr lnr-map-marker pdico"></span>
                </div>
                <div class="col-7" v-html="location"></div>
                <div class="col-3 ">
                    <a href="#" class="changelink" data-toggle="modal" data-target="#delivery_locations">Change</a>
                </div>
            </div>
            <div class="row dr">

                <div class="col-2 "><span class="lnr lnr-history pdico"></span>
                </div>
                <div class="col-7"> Home Delivery
                    <br>
                    <small v-html="total_delivery_duration"></small>
                </div>
                <div class="col-3">
                    <small><b v-html="total_delivery_charge">Rs. 159</b></small>
                </div>


            </div>

            <div class="row dr" v-if="money_back_guarantee != 'No'">
                <div class="col-md-2 mt6"><span class="lnr lnr-gift pdico"></span>
                </div>
                <div class="col-7 mt6" v-if="is_cash_on_delivery_available"> Cash on Delivery Available</div>
                <div class="col-7 mt6" v-else> Only Card Payments Accepted</div>

                <div class="col-3">

                </div>

            </div>
            <div class="row dr" v-if="money_back_guarantee != 'No'">
{{--                <div class="col-md-12">--}}
{{--                    <h6 class="phead">Return</h6>--}}
{{--                </div>--}}
                <div class="col-md-2 "><span class="lnr lnr-checkmark-circle pdico"></span>
                </div>
                <div class="col-7 "> 7 Days Returns
                    <br>
                    <small>Change of mind is not applicable</small>

                </div>

            </div>

            <div class="row dr" v-if="warranty != 'No Warranty'">
{{--                <div class="col-md-12">--}}
{{--                    <h6 class="phead">Warranty</h6>--}}
{{--                </div>--}}
                <div class="col-md-2 "><span class="lnr lnr-hand pdico"></span>
                </div>

                <div class="col-md-7  "><b v-html="warranty"></b> Warranty Available <br>
                    <small>* Terms and conditions may apply</small>

                </div>
                <div class="col-3 mt6">

                </div>

            </div>

            <div class="row dr">
                <div class="col-md-12">
                    {!! view_render_event('bagisto.shop.products.view.description.before', ['product' => $product]) !!}
                </div>

            </div>
            <!-- Modal -->
            <div class="modal fade" id="delivery_locations" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Change Address</h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <p v-if="location_bread_crumb" v-html="location_bread_crumb"
                                   style=" padding: 5px; background: #eee;margin-bottom: 10px;font-size: 12px;color: #2635e3;"></p>
                                <div class="form-group">
                                    <label for="">Province</label>
                                    <select name="" id="select_location_province" class="form-control"
                                            v-model="selected_province" @change="fetchDistricts($event)"
                                            live-search="true">
                                        <option value="">Select Province</option>
                                        <option v-for="province in province_list" :value="province.id"
                                                v-html="province.province_name"></option>
                                    </select>
                                </div>

                                <div class="form-group" v-if="selected_province && district_list.length">
                                    <label for="">District</label>
                                    <select name="" id="" class="form-control" v-model="selected_district"
                                            @change="fetchCities($event)">
                                        <option value="">Select District</option>
                                        <option v-for="dist in district_list" :value="dist.id"
                                                v-html="dist.district_name"></option>
                                    </select>
                                </div>

                                <div class="form-group" v-if="selected_district && city_list.length">
                                    <label for="">City</label>
                                    <select name="" id="" class="form-control" v-model="selected_city"
                                            @change="selectedCities($event)">
                                        <option value="">Select City</option>
                                        <option v-for="city in city_list" :value="city.id"
                                                v-html="city.city_name"></option>
                                    </select>
                                </div>

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                    id="delivery_locations_close">Close
                            </button>
                            <button type="button" class="btn btn-primary" :disabled="!selected_city"
                                    @click="saveChanges()">Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </script>

    <script>
        Vue.component('product-view', {
            inject: ['$validator'],
            template: '#product-view-template',
            data: function () {
                return {
                    slot: true,
                    is_buy_now: 0,

                }
            },

            mounted: function () {
                let currentProductId = '{{ $product->url_key }}';
                let existingViewed = window.localStorage.getItem('recentlyViewed');

                if (!existingViewed) {
                    existingViewed = [];
                } else {
                    existingViewed = JSON.parse(existingViewed);
                }

                if (existingViewed.indexOf(currentProductId) == -1) {
                    existingViewed.push(currentProductId);

                    if (existingViewed.length > 3)
                        existingViewed = existingViewed.slice(Math.max(existingViewed.length - 4, 1));

                    window.localStorage.setItem('recentlyViewed', JSON.stringify(existingViewed));
                } else {
                    var uniqueNames = [];

                    $.each(existingViewed, function (i, el) {
                        if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
                    });

                    uniqueNames.push(currentProductId);

                    uniqueNames.splice(uniqueNames.indexOf(currentProductId), 1);

                    window.localStorage.setItem('recentlyViewed', JSON.stringify(uniqueNames));
                }
            },

            methods: {
                onSubmit: function (event) {
                    if (event.target.getAttribute('type') != 'submit')
                        return;

                    event.preventDefault();

                    this.$validator.validateAll().then(result => {
                        if (result) {
                            this.is_buy_now = event.target.classList.contains('buynow') ? 1 : 0;

                            setTimeout(function () {
                                document.getElementById('product-form').submit();
                            }, 0);
                        }
                    });
                },
            }
        });
        Vue.component('delivery', {
            template: '#delivery-template',
            data: function () {
                return {
                    location_province: 'Western Province',
                    location_district: 'Colombo',
                    location_city: 'Colombo 1',
                    location: '',
                    delivery_charge: 150.00,
                    additional_delivery_charge: 50,
                    total_additional_delivery_charge: 0,
                    delivery_currency: "Rs. ",
                    total_delivery_charge: '',
                    delivery_duration: "2-3",
                    delivery_duration_unit: "Day(s)",
                    total_delivery_duration: '',
                    is_cash_on_delivery_available: false,
                    province_list: [],
                    district_list: [],
                    city_list: [],
                    selected_province: '',
                    selected_district: '',
                    selected_city: '',
                    selected_province_name: '',
                    selected_district_name: '',
                    selected_city_name: '',
                    location_bread_crumb: '',
                    weight: {{($product->weight)? $product->weight : 0}},
                    warranty: '{{$warranty}}',
                    money_back_guarantee: '{{$money_back_guarantee}}',

                }
            },

            mounted: function () {
                let self = this;
                this.$http.get(`${this.baseUrl}/province/all`)
                    .then(response => {
                        self.province_list = response.data;
                    })
                    .catch(error => {
                        console.log('something went wrong');
                    });
                this.calcLocation();
                this.calcDeliveryDuration();
                this.calcDeliveryCharge();
                console.log(this.weight);

            },

            methods: {
                calcDeliveryDuration() {
                    this.total_delivery_duration = this.delivery_duration + ' ' + this.delivery_duration_unit;
                },
                calcDeliveryCharge() {
                    this.total_additional_delivery_charge = (this.weight - 1) * this.additional_delivery_charge;
                    this.total_delivery_charge = this.delivery_currency + ' ' + this.delivery_charge + ' +<br>' + ' ' + this.delivery_currency + ' ' + this.total_additional_delivery_charge;
                },
                calcLocation() {
                    this.location = this.location_province + ' Province, ' + this.location_district + ',<br> ' + this.location_city;
                },
                fetchDistricts(event) {
                    let self = this;

                    self.city_list = [];
                    self.selected_city_name = '';
                    self.selected_city = '';

                    self.district_list = [];
                    self.selected_district_name = '';
                    self.selected_district = '';

                    if (this.selected_province != '') {
                        this.selected_province_name = event.target.options[event.target.options.selectedIndex].text;
                        this.$http.get(`${this.baseUrl}/districts/all/` + this.selected_province)
                            .then(response => {
                                self.district_list = response.data;
                            })
                            .catch(error => {
                                console.log('something went wrong');
                            });
                    } else {
                        self.district_list = [];
                        self.selected_district_name = '';
                        self.selected_district = '';
                        self.selected_province_name = '';
                        self.selected_province = '';

                        self.city_list = [];
                        self.selected_city_name = '';
                        self.selected_city = '';

                    }

                    this.buildBreadCrumb();
                },
                fetchCities(event) {
                    let self = this;

                    self.city_list = [];
                    self.selected_city_name = '';
                    self.selected_city = '';

                    if (this.selected_district != '') {
                        this.selected_district_name = event.target.options[event.target.options.selectedIndex].text;
                        this.$http.get(`${this.baseUrl}/cities/all/` + this.selected_district)
                            .then(response => {
                                self.city_list = response.data;
                            })
                            .catch(error => {
                                console.log('something went wrong');
                            });
                    } else {
                        self.district_list = [];
                        self.selected_district_name = '';
                        self.selected_district = '';

                        self.city_list = [];
                        self.selected_city_name = '';
                        self.selected_city = '';
                    }

                    this.buildBreadCrumb();

                },
                selectedCities(event) {
                    this.selected_city_name = event.target.options[event.target.options.selectedIndex].text;
                    this.buildBreadCrumb();
                },
                buildBreadCrumb() {
                    let breadCrumb = '';

                    if (this.selected_province_name != '') {
                        breadCrumb += this.selected_province_name + ' Province > ';
                    }

                    if (this.selected_district_name != '') {
                        breadCrumb += this.selected_district_name + ' District > ';
                    }

                    if (this.selected_city_name != '') {
                        breadCrumb += this.selected_city_name;
                    }

                    this.location_bread_crumb = breadCrumb;
                },
                saveChanges() {
                    this.location_province = this.selected_province_name;
                    this.location_district = this.selected_district_name;
                    this.location_city = this.selected_city_name;

                    this.calcLocation();
                    this.getDeliveryCharge();

                },
                getDeliveryCharge() {
                    let self = this;
                    this.$http.get(`${this.baseUrl}/delivery-charge/get/` + this.selected_city)
                        .then(response => {
                            console.log(response);

                            self.delivery_charge = response.data.location_groups.delivery_charge;
                            self.additional_delivery_charge = response.data.location_groups.additional_kg;
                            self.delivery_currency = response.data.location_groups.currency;
                            self.delivery_duration = response.data.location_groups.dilivery_days;
                            self.delivery_duration_unit = response.data.location_groups.delivery_days_unit;
                            self.is_cash_on_delivery_available = response.data.location_groups.is_cod_available;

                            self.calcDeliveryDuration();
                            self.calcDeliveryCharge();
                            document.getElementById('delivery_locations_close').click();
                        })
                        .catch(error => {
                            console.log(error);
                            console.log('something went wrong');
                        });
                }

            }
        });

        window.onload = function () {
            var thumbList = document.getElementsByClassName('thumb-list')[0];
            var thumbFrame = document.getElementsByClassName('thumb-frame');
            var productHeroImage = document.getElementsByClassName('product-hero-image')[0];

            if (thumbList && productHeroImage) {
                for (let i = 0; i < thumbFrame.length; i++) {
                    thumbFrame[i].style.height = (productHeroImage.offsetHeight / 4) + "px";
                    thumbFrame[i].style.width = (productHeroImage.offsetHeight / 4) + "px";
                }

                if (screen.width > 720) {
                    thumbList.style.width = (productHeroImage.offsetHeight / 4) + "px";
                    thumbList.style.minWidth = (productHeroImage.offsetHeight / 4) + "px";
                    thumbList.style.height = productHeroImage.offsetHeight + "px";
                }
            }

            window.onresize = function () {
                if (thumbList && productHeroImage) {

                    for (let i = 0; i < thumbFrame.length; i++) {
                        thumbFrame[i].style.height = (productHeroImage.offsetHeight / 4) + "px";
                        thumbFrame[i].style.width = (productHeroImage.offsetHeight / 4) + "px";
                    }

                    if (screen.width > 720) {
                        thumbList.style.width = (productHeroImage.offsetHeight / 4) + "px";
                        thumbList.style.minWidth = (productHeroImage.offsetHeight / 4) + "px";
                        thumbList.style.height = productHeroImage.offsetHeight + "px";
                    }
                }
            }
        };
    </script>
@endpush