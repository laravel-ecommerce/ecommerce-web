@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.login-form.page-title') }}
@endsection

@section('content-wrapper')
    <div class="auth-content r">

        {!! view_render_event('bagisto.shop.customers.login.before') !!}

        <div class="container">
            <div class="col-md-12">
                <div class="card centercard">
                    <div class="card-body">
                        <div class="heading">
                            <h2 class="fs24 fw6">
                                Welcome to Ceibee! Please login.
                            </h2>


                        </div>
                        <form
                            method="POST"
                            action="{{ route('customer.session.create') }}"
                            @submit.prevent="onSubmit">
                        <div class="row">
                        <div class="body col-6">



                            {{ csrf_field() }}



                            <div class="form-group" :class="[errors.has('email') ? 'has-error' : '']">
                                <label for="email" class="mandatory label-style">
                                    {{ __('shop::app.customer.login-form.email') }}
                                </label>

                                <input
                                    type="text"
                                    class="form-style"
                                    name="email"
                                    v-validate="'required|email'"
                                    value="{{ old('email') }}"
                                    data-vv-as="&quot;{{ __('shop::app.customer.login-form.email') }}&quot;"/>

                                <span class="control-error" v-if="errors.has('email')">
                                    @{{ errors.first('email') }}
                                </span>
                            </div>

                            <div class="form-group" :class="[errors.has('password') ? 'has-error' : '']">
                                <label for="password" class="mandatory label-style">
                                    {{ __('shop::app.customer.login-form.password') }}
                                </label>

                                <input
                                    type="password"
                                    class="form-style"
                                    name="password"
                                    v-validate="'required'"
                                    value="{{ old('password') }}"
                                    data-vv-as="&quot;{{ __('shop::app.customer.login-form.password') }}&quot;"/>

                                <span class="control-error" v-if="errors.has('password')">
                                    @{{ errors.first('password') }}
                                </span>

                                <a href="{{ route('customer.forgot-password.create') }}" class="float-right">
                                    {{ __('shop::app.customer.login-form.forgot_pass') }}
                                </a>

                                <div class="mt10">
                                    @if (Cookie::has('enable-resend'))
                                        @if (Cookie::get('enable-resend') == true)
                                            <a href="{{ route('customer.resend.verification-email', Cookie::get('email-for-resend')) }}">{{ __('shop::app.customer.login-form.resend-verification') }}</a>
                                        @endif
                                    @endif
                                </div>
                            </div>

                            {!! view_render_event('bagisto.shop.customers.login_form_controls.after') !!}

                            <input class="theme-btn" type="submit"
                                   value="Login">


                    </div>
                        <div class="body col-6">
                            {!! view_render_event('bagisto.shop.customers.login_form_controls.before') !!}


                            <a href="{{ route('customer.register.index') }}" class="btn-new-customer">
                                <button type="button" class="theme-btn light">
                                    New member? Register here
                                </button>
                            </a>
                        </div>

                </div>
                        </form>
                </div>
            </div>
        </div>
    </div>

    {!! view_render_event('bagisto.shop.customers.login.after') !!}
    </div>
@endsection
