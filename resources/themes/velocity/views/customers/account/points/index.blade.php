@extends('shop::customers.account.index')

@section('page_title')
    {{ __('shop::app.customer.account.points.title') }}
@endsection

@section('page-detail-wrapper')
    {{--    <div class="account-head mb-10">--}}
    {{--        <span class="back-icon"><a href="{{ route('customer.account.index') }}"><i class="icon icon-menu-back"></i></a></span>--}}
    {{--        <span class="account-heading">--}}
    {{--            Points--}}
    {{--        </span>--}}

    {{--        <div class="horizontal-rule"></div>--}}
    {{--    </div>--}}
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center invite-heding animated infinite headShake">
                Invite friends
            </h1>
            <h4 class="text-center invite-sub-heding ">
                Invite friends to join <b>Ceibee.com</b>! <br> Once they do a purchase, you'll also get points.
            </h4>
            <br>
            <div class="rax-view legacy-inviteActivity-Invite-container">
                <div class="rax-view legacy-inviteActivity-Invite-content">
                    <div class="rax-view share-link">
                        <h4 class="text-center animated infinite pulse ">Share your referral link:</h4>

                        <div class="input-group mb-3">
                            <input type="text" class="form-control reflink-input"
                                   readonly value="{{route('customer.invite.accept',['token'=>$user->ref_token])}}"
                                   id="copyTarget">
                            <div class="input-group-append">
                                <button class="btn btn-warning" type="button"  id="copyButton"
                                        onclick="document.getElementById('copyTarget').select();
                                        document.execCommand('copy');
                                        document.getElementById('copyButton').innerHTML='Copied';
                                        setTimeout(function (){document.getElementById('copyButton').innerHTML='Copy';},1000)"
                                >Copy</button>
                            </div>
                        </div>
                    </div>
                    <div class="rax-view detail-banner ">
                        <div class="rax-view detail-banner-title-wrap">
                            <div class="rax-view detail-banner-title">Your Earned Points</div>
                        </div>
                        <span class="rax-text detail-banner-btn">Points {{$user->points}}</span></div>
                </div>
            </div>

            <div class="rax-view legacy-inviteActivity-Invite-container">
                <a class="btn btn-warning btn btn-block" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    Registerd Referrals
                </a>
                <div class="collapse" id="collapseExample">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>User Level</th>
                        </tr>
                        @if(count($refList))
                            @php $x= 1; @endphp
                            @foreach($refList as $ref)
                                <tr>
                                    <td>{{$x}}</td>
                                    <td>{{$ref->refereer->first_name.' '.$ref->refereer->last_name}}</td>
                                    <td>Level <b>{{$ref->level}}</b></td>
                                </tr>
                                @php $x++; @endphp
                            @endforeach

                        @else
                            <tr>
                                <td colspan="4">No records</td>
                            </tr>
                        @endif

                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection

@prepend('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <style>
        .account-content{
            height: auto !important;
        }
        .account-layout {
            background: rgb(245, 61, 45);
            background: linear-gradient(40deg, rgba(245, 61, 45, 1) 35%, rgba(255, 156, 51, 1) 100%);
            border-radius: 0 50px 0;
            margin-right: 7px;
        }

        .velocity-divide-page .left {
            background: #fff !important;
        }

        .invite-heding {
            font-weight: 700;
            color: #fff;
            margin: 15px 0 22px;
        }

        .invite-sub-heding {
            font-weight: 700;
            color: #fff;
            margin-bottom: 15px;
        }

        .legacy-inviteActivity-Invite-container {
            box-sizing: border-box;
            border-radius: 12px;
            padding: 27px 85px 32px;
            position: relative;
            width: 720px;
            margin: 0 auto 20px;
            background: #fff;
        }

        .reflink-input {
            border: 2px dashed #222;
            height: 55px;
            font-size: 18px;
        }

        .detail-banner {
            display: flex;
            flex-direction: row;
            width: 644px;
            height: 220px;
            padding: 0 60px 20px;
            margin-left: -46px;
            margin-top: 20px;
            justify-content: space-between;
            align-items: flex-end;
            position: relative;
            background-image: url(/vendor/webkul/ui/assets/images/points-bg.png);
            background-repeat: no-repeat;
            background-size: 100% 100%;
            cursor: pointer;
        }

        .detail-banner .detail-banner-title-wrap {
            display: block;
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
        }

        .detail-banner .detail-banner-title-wrap .detail-banner-subtitle {
            line-height: 33px;
            font-size: 24px;
            font-weight: 700;
            color: #fff;
        }

        .detail-banner .detail-banner-btn {
            min-width: 150px;
            height: 60px;
            display: flex;
            padding: 0 8px;
            justify-content: center;
            align-items: center;
            border-radius: 30px;
            font-size: 21px;
            font-weight: 700;
            color: #000;
            background-color: #fff;
        }

        .detail-banner .detail-banner-title-wrap .detail-banner-title {
            width: 333px;
            height: 90px;
            margin-bottom: 12px;
            line-height: 60px;
            font-size: 30px;
            font-weight: 900;
            color: #fff;
        }
    </style>
@endprepend
