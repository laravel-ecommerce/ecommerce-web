@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.signup-form.page-title') }}
@endsection

@section('content-wrapper')
    <div class="auth-content ">
        <div class="container">
            <div class="col-lg-12">
                <div class="heading">


                </div>
                <div class="card centercard">
                    <div class="card-body">
                        <form
                                method="post"
                                action="{{ route('customer.register.create') }}"
                                @submit.prevent="onSubmit">

                            {{ csrf_field() }}
                            <div class="row">


                                <div class="body col-md-6">

                                    <h3 class="rgh">Create your Ceibee Account</h3>


                                    {!! view_render_event('bagisto.shop.customers.signup.before') !!}



                                    {!! view_render_event('bagisto.shop.customers.signup_form_controls.before') !!}

                                    <div class="control-group" :class="[errors.has('first_name') ? 'has-error' : '']">
                                        <label for="first_name" class="required label-style">
                                            {{ __('shop::app.customer.signup-form.firstname') }}
                                        </label>

                                        <input
                                                type="text"
                                                class="form-style"
                                                name="first_name"
                                                v-validate="'required'"
                                                value="{{ old('first_name') }}"
                                                data-vv-as="&quot;{{ __('shop::app.customer.signup-form.firstname') }}&quot;"/>

                                        <span class="control-error" v-if="errors.has('first_name')">
                                @{{ errors.first('first_name') }}
                            </span>
                                    </div>

                                    {!! view_render_event('bagisto.shop.customers.signup_form_controls.firstname.after') !!}

                                    <div class="control-group" :class="[errors.has('last_name') ? 'has-error' : '']">
                                        <label for="last_name" class="required label-style">
                                            {{ __('shop::app.customer.signup-form.lastname') }}
                                        </label>

                                        <input
                                                type="text"
                                                class="form-style"
                                                name="last_name"
                                                v-validate="'required'"
                                                value="{{ old('last_name') }}"
                                                data-vv-as="&quot;{{ __('shop::app.customer.signup-form.lastname') }}&quot;"/>

                                        <span class="control-error" v-if="errors.has('last_name')">
                                @{{ errors.first('last_name') }}
                            </span>
                                    </div>

                                    {!! view_render_event('bagisto.shop.customers.signup_form_controls.lastname.after') !!}

                                    <div class="control-group" :class="[errors.has('email') ? 'has-error' : '']">
                                        <label for="email" class="required label-style">
                                            {{ __('shop::app.customer.signup-form.email') }}
                                        </label>

                                        <input
                                                type="email"
                                                class="form-style"
                                                name="email"
                                                v-validate="'required|email'"
                                                value="{{ old('email') }}"
                                                data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;"/>

                                        <span class="control-error" v-if="errors.has('email')">
                                @{{ errors.first('email') }}
                            </span>
                                    </div>

                                    {!! view_render_event('bagisto.shop.customers.signup_form_controls.email.after') !!}


                                </div>
                                <div class="body col-md-6">

                                    <div class="control-group" :class="[errors.has('password') ? 'has-error' : '']">
                                        <label for="password" class="required label-style">
                                            {{ __('shop::app.customer.signup-form.password') }}
                                        </label>

                                        <input
                                                type="password"
                                                class="form-style"
                                                name="password"
                                                v-validate="'required|min:6'"
                                                ref="password"
                                                value="{{ old('password') }}"
                                                data-vv-as="&quot;{{ __('shop::app.customer.signup-form.password') }}&quot;"/>

                                        <span class="control-error" v-if="errors.has('password')">
                                @{{ errors.first('password') }}
                            </span>
                                    </div>

                                    {!! view_render_event('bagisto.shop.customers.signup_form_controls.password.after') !!}

                                    <div class="control-group"
                                         :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                                        <label for="password_confirmation" class="required label-style">
                                            {{ __('shop::app.customer.signup-form.confirm_pass') }}
                                        </label>

                                        <input
                                                type="password"
                                                class="form-style"
                                                name="password_confirmation"
                                                v-validate="'required|min:6|confirmed:password'"
                                                data-vv-as="&quot;{{ __('shop::app.customer.signup-form.confirm_pass') }}&quot;"/>

                                        <span class="control-error" v-if="errors.has('password_confirmation')">
                                @{{ errors.first('password_confirmation') }}
                            </span>
                                    </div>
                                    <input type="text" name="ref" value="{{app('request')->input('ref')}}" hidden>

                                    @if (core()->getConfigData('customer.settings.newsletter.subscription'))
                                        <div class="control-group">
                                            <input type="checkbox" id="checkbox2" name="is_subscribed">
                                            <span>{{ __('shop::app.customer.signup-form.subscribe-to-newsletter') }}</span>
                                        </div>
                                    @endif

                                    {!! view_render_event('bagisto.shop.customers.signup_form_controls.after') !!}

                                    <button class="theme-btn" type="submit">
                                        Register Now
                                    </button>


                                    {!! view_render_event('bagisto.shop.customers.signup.after') !!}
                                    <p class="logtxt">Or you already have Account? Login here</p>
                                    <a href="{{ route('customer.session.index') }}" class="btn-new-customer">
                                        <button type="button" class="theme-btn light">
                                            {{ __('velocity::app.customer.signup-form.login')}}
                                        </button>
                                    </a>
                                </div>


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
