<nav class="row" id="top">
    <div class="col-sm-6">
{{--        @include('velocity::layouts.top-nav.locale-currency')--}}
    </div>

    <div class="col-sm-4 text-right">


        <div class="d-inline-block pointsblock welcome-content">
            <a href="{{route('customer.invite.index')}}" class="float-right unset">
                <span class="material-icons-outlined point"></span>
                <span class="text-center">Invite Friends and Earn Points</span></a>
        </div>
    </div>
    <div class="col-sm-2">
        @include('velocity::layouts.top-nav.login-section')
    </div>
</nav>