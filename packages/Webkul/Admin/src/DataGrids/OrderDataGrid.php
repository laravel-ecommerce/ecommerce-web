<?php

namespace Webkul\Admin\DataGrids;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Webkul\Sales\Models\OrderAddress;
use Webkul\Ui\DataGrid\DataGrid;

class OrderDataGrid extends DataGrid
{

    protected $index = 'id';

    protected $sortOrder = 'desc';

    protected $from_date = null;

    protected $to_date = null;

    protected $status = null;

    public function __construct()
    {
        parent::__construct();

        $this->from_date = (request()->get('from_date') != '') ? request()->get('from_date') ?? null : null;

        $this->to_date = (request()->get('from_date') != request()->get('to_date')) ? request()->get('to_date') ?? null : null;

        $this->status = (request()->get('status') != '') ? request()->get('status') ?? null : null;

    }


    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('orders')
            ->leftJoin('addresses as order_address_shipping', function($leftJoin) {
                $leftJoin->on('order_address_shipping.order_id', '=', 'orders.id')
                         ->where('order_address_shipping.address_type', OrderAddress::ADDRESS_TYPE_SHIPPING);
            })
            ->leftJoin('addresses as order_address_billing', function($leftJoin) {
                $leftJoin->on('order_address_billing.order_id', '=', 'orders.id')
                         ->where('order_address_billing.address_type', OrderAddress::ADDRESS_TYPE_BILLING);
            })
            ->leftJoin('order_items', function($leftJoin) {
                $leftJoin->on('order_items.order_id', '=', 'orders.id');
            })
//            ->leftJoin('order_items as order_items', 'order_items.order_id', '=', 'orders.id')
            ->addSelect('order_items.name','order_items.sku','orders.total_qty_ordered', 'orders.created_at','orders.id','orders.increment_id','order_items.product_id', 'orders.base_sub_total',
                'orders.base_grand_total', 'channel_name', 'status')
            ->addSelect(DB::raw('CONCAT(' . DB::getTablePrefix() . 'order_address_billing.first_name, " ", ' . DB::getTablePrefix() . 'order_address_billing.last_name) as billed_to'))
            ->addSelect(DB::raw('CONCAT(' . DB::getTablePrefix() . 'order_address_shipping.first_name, " ", ' . DB::getTablePrefix() . 'order_address_shipping.last_name) as shipped_to'))
            ->addSelect('orders.shipping_method');

        $this->addFilter('billed_to', DB::raw('CONCAT(' . DB::getTablePrefix() . 'order_address_billing.first_name, " ", ' . DB::getTablePrefix() . 'order_address_billing.last_name)'));
        $this->addFilter('shipped_to', DB::raw('CONCAT(' . DB::getTablePrefix() . 'order_address_shipping.first_name, " ", ' . DB::getTablePrefix() . 'order_address_shipping.last_name)'));
        $this->addFilter('increment_id', 'orders.increment_id');
        $this->addFilter('created_at', 'orders.created_at');

        if(!empty($this->status)){
            $queryBuilder->where('orders.status','=',$this->status);
        }

        if(!empty($this->from_date) AND empty($this->to_date)){
            $queryBuilder->whereDate('orders.created_at','=',$this->from_date);
        }elseif (!empty($this->from_date) AND !empty($this->to_date)){
            $queryBuilder->whereBetween('orders.created_at',[Carbon::parse($this->from_date)->format('Y-m-d H:i:s'),Carbon::parse($this->to_date)->format('Y-m-d H:i:s')]);
        }

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'id',
            'label'      => '#',
            'type'       => 'string',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
            'closure'    => true,
            'wrapper'    => function ($value) {
                return '<input type="checkbox" class="form-control" name="bulk_ids" value="'.$value->id.'">';
            },
        ]);
        $this->addColumn([
            'index'      => 'increment_id',
            'label'      => trans('admin::app.datagrid.id'),
            'type'       => 'string',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'base_sub_total',
            'label'      => trans('admin::app.datagrid.sub-total'),
            'type'       => 'price',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'base_grand_total',
            'label'      => trans('admin::app.datagrid.grand-total'),
            'type'       => 'price',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'created_at',
            'label'      => trans('admin::app.datagrid.order-date'),
            'type'       => 'datetime',
            'sortable'   => true,
            'searchable' => false,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'channel_name',
            'label'      => trans('admin::app.datagrid.channel-name'),
            'type'       => 'string',
            'sortable'   => true,
            'searchable' => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'status',
            'label'      => trans('admin::app.datagrid.status'),
            'type'       => 'string',
            'sortable'   => true,
            'searchable' => true,
            'closure'    => true,
            'filterable' => true,
            'wrapper' => function ($value) {
                if ($value->status == 'processing') {
                    return '<span class="badge badge-md badge-success">'. trans('admin::app.sales.orders.order-status-processing') .'</span>';
                } elseif ($value->status == 'completed') {
                    return '<span class="badge badge-md badge-success">'. 'Shipped' .'</span>';
                } elseif ($value->status == "canceled") {
                    return '<span class="badge badge-md badge-danger">'. trans('admin::app.sales.orders.order-status-canceled') .'</span>';
                } elseif ($value->status == "closed") {
                    return '<span class="badge badge-md badge-info">'. trans('admin::app.sales.orders.order-status-closed') .'</span>';
                } elseif ($value->status == "pending") {
                    return '<span class="badge badge-md badge-warning">'. trans('admin::app.sales.orders.order-status-pending') .'</span>';
                } elseif ($value->status == "pending_payment") {
                    return '<span class="badge badge-md badge-warning">'. trans('admin::app.sales.orders.order-status-pending-payment') .'</span>';
                } elseif ($value->status == "fraud") {
                    return '<span class="badge badge-md badge-danger">'. trans('admin::app.sales.orders.order-status-fraud') . '</span>';
                }elseif ($value->status == "delivered") {
                    return '<span class="badge badge-md badge-info" style="background: background: #c52ee1 !important;">'. 'Delivered' . '</span>';
                }
            },
        ]);

        $this->addColumn([
            'index'      => 'billed_to',
            'label'      => trans('admin::app.datagrid.billed-to'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'shipped_to',
            'label'      => trans('admin::app.datagrid.shipped-to'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'title'  => trans('admin::app.datagrid.view'),
            'method' => 'GET',
            'route'  => 'admin.sales.orders.view',
            'icon'   => 'icon eye-icon',
        ]);
        
    }
}