<?php
return [
    [
        'key' => 'general',
        'name' => 'admin::app.admin.system.general',
        'sort' => 1,
    ], [
        'key' => 'general.general',
        'name' => 'admin::app.admin.system.general',
        'sort' => 1,
    ], [
        'key' => 'general.general.locale_options',
        'name' => 'admin::app.admin.system.locale-options',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'weight_unit',
                'title' => 'admin::app.admin.system.weight-unit',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'lbs',
                        'value' => 'lbs',
                    ], [
                        'title' => 'kgs',
                        'value' => 'kgs',
                    ],
                ],
                'channel_based' => true
            ],
        ],
    ], [
        'key' => 'general.general.email_settings',
        'name' => 'admin::app.admin.system.email-settings',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'sender_name',
                'title' => 'admin::app.admin.system.email-sender-name',
                'type' => 'text',
                'validation' => 'required|max:50',
                'channel_based' => true,
                'default_value' => config('mail.from.name'),
            ], [
                'name' => 'shop_email_from',
                'title' => 'admin::app.admin.system.shop-email-from',
                'type' => 'text',
                'validation' => 'required|email',
                'channel_based' => true,
                'default_value' => config('mail.from.address'),
            ], [
                'name' => 'admin_name',
                'title' => 'admin::app.admin.system.admin-name',
                'type' => 'text',
                'validation' => 'required|max:50',
                'channel_based' => true,
                'default_value' => config('mail.admin.name'),
            ], [
                'name' => 'admin_email',
                'title' => 'admin::app.admin.system.admin-email',
                'type' => 'text',
                'validation' => 'required|email',
                'channel_based' => true,
                'default_value' => config('mail.admin.address'),
            ],
        ],
    ], [
        'key' => 'general.content',
        'name' => 'admin::app.admin.system.content',
        'sort' => 2,
    ], [
        'key' => 'general.content.footer',
        'name' => 'admin::app.admin.system.footer',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'footer_content',
                'title' => 'admin::app.admin.system.footer-content',
                'type' => 'text',
                'channel_based' => true,
                'locale_based' => true,
            ], [
                'name' => 'footer_toggle',
                'title' => 'admin::app.admin.system.footer-toggle',
                'type' => 'boolean',
                'locale_based' => true,
                'channel_based' => true,
            ],
        ],
    ], [
        'key' => 'general.content.custom_scripts',
        'name' => 'admin::app.admin.system.custom-scripts',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'custom_css',
                'title' => 'admin::app.admin.system.custom-css',
                'type' => 'textarea',
                'channel_based' => true,
                'locale_based' => false,
            ], [
                'name' => 'custom_javascript',
                'title' => 'admin::app.admin.system.custom-javascript',
                'type' => 'textarea',
                'channel_based' => true,
                'locale_based' => false,
            ]
        ],
    ], [
        'key' => 'general.design',
        'name' => 'admin::app.admin.system.design',
        'sort' => 3,
    ], [
        'key' => 'general.design.admin_logo',
        'name' => 'admin::app.admin.system.admin-logo',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'logo_image',
                'title' => 'admin::app.admin.system.logo-image',
                'type' => 'image',
                'channel_based' => true,
                'validation' => 'mimes:bmp,jpeg,jpg,png,webp',
            ], [
                'name' => 'favicon',
                'title' => 'admin::app.admin.system.favicon',
                'type' => 'image',
                'channel_based' => true,
                'validation' => 'mimes:bmp,jpeg,jpg,png,webp',
            ],
        ],
    ], [
        'key' => 'catalog',
        'name' => 'admin::app.admin.system.catalog',
        'sort' => 2,
    ], [
        'key' => 'catalog.products',
        'name' => 'admin::app.admin.system.products',
        'sort' => 2,
    ], [
        'key' => 'catalog.products.guest-checkout',
        'name' => 'admin::app.admin.system.guest-checkout',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'allow-guest-checkout',
                'title' => 'admin::app.admin.system.allow-guest-checkout',
                'type' => 'boolean',
            ],
        ],
    ], [
        'key' => 'catalog.products.homepage',
        'name' => 'admin::app.admin.system.homepage',
        'sort' => 2,
        'fields' => [
            [
                'name' => 'no_of_new_product_homepage',
                'title' => 'admin::app.admin.system.allow-no-of-new-product-homepage',
                'type' => 'number',
                'validation' => 'min:0',
            ],
            [
                'name' => 'no_of_featured_product_homepage',
                'title' => 'admin::app.admin.system.allow-no-of-featured-product-homepage',
                'type' => 'number',
                'validation' => 'min:0',
            ],
            [
                'name' => 'out_of_stock_items',
                'title' => 'admin::app.admin.system.allow-out-of-stock-items',
                'type' => 'boolean',
            ],
        ],
    ], [
        'key' => 'catalog.products.storefront',
        'name' => 'admin::app.admin.system.storefront',
        'sort' => 2,
        'fields' => [
            [
                'name' => 'mode',
                'title' => 'admin::app.admin.system.default-list-mode',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'admin::app.admin.system.grid',
                        'value' => 'grid',
                    ], [
                        'title' => 'admin::app.admin.system.list',
                        'value' => 'list',
                    ],
                ],
                'channel_based' => true,
            ], [
                'name' => 'products_per_page',
                'title' => 'admin::app.admin.system.products-per-page',
                'type' => 'text',
                'info' => 'admin::app.admin.system.comma-seperated',
                'channel_based' => true,
            ], [
                'name' => 'sort_by',
                'title' => 'admin::app.admin.system.sort-by',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'admin::app.admin.system.from-z-a',
                        'value' => 'name-desc',
                    ], [
                        'title' => 'admin::app.admin.system.from-a-z',
                        'value' => 'name-asc',
                    ], [
                        'title' => 'admin::app.admin.system.newest-first',
                        'value' => 'created_at-desc',
                    ], [
                        'title' => 'admin::app.admin.system.oldest-first',
                        'value' => 'created_at-asc',
                    ], [
                        'title' => 'admin::app.admin.system.cheapest-first',
                        'value' => 'price-asc',
                    ], [
                        'title' => 'admin::app.admin.system.expensive-first',
                        'value' => 'price-desc',
                    ],
                ],
                'channel_based' => true,
            ], [
                'name' => 'buy_now_button_display',
                'title' => 'admin::app.admin.system.buy-now-button-display',
                'type' => 'boolean',
            ]
        ],
    ], [
        'key' => 'catalog.products.review',
        'name' => 'admin::app.admin.system.review',
        'sort' => 3,
        'fields' => [
            [
                'name' => 'guest_review',
                'title' => 'admin::app.admin.system.allow-guest-review',
                'type' => 'boolean',
            ],
        ],
    ], [
        'key' => 'catalog.products.attribute',
        'name' => 'admin::app.admin.system.attribute',
        'sort' => 4,
        'fields' => [
            [
                'name' => 'image_attribute_upload_size',
                'title' => 'admin::app.admin.system.image-upload-size',
                'type' => 'text',
            ], [
                'name' => 'file_attribute_upload_size',
                'title' => 'admin::app.admin.system.file-upload-size',
                'type' => 'text',
            ]
        ],
    ], [
        'key' => 'catalog.inventory',
        'name' => 'admin::app.admin.system.inventory',
        'sort' => 1,
    ], [
        'key' => 'catalog.inventory.stock_options',
        'name' => 'admin::app.admin.system.stock-options',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'backorders',
                'title' => 'admin::app.admin.system.allow-backorders',
                'type' => 'boolean',
                'channel_based' => true,
            ],
        ],
    ], [
        'key' => 'catalog.rich_snippets',
        'name' => 'admin::app.admin.system.rich-snippets',
        'sort' => 3,
    ], [
        'key' => 'catalog.rich_snippets.products',
        'name' => 'admin::app.admin.system.products',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'enable',
                'title' => 'admin::app.admin.system.enable',
                'type' => 'boolean',
            ], [
                'name' => 'show_sku',
                'title' => 'admin::app.admin.system.show-sku',
                'type' => 'boolean',
            ], [
                'name' => 'show_weight',
                'title' => 'admin::app.admin.system.show-weight',
                'type' => 'boolean',
            ], [
                'name' => 'show_categories',
                'title' => 'admin::app.admin.system.show-categories',
                'type' => 'boolean',
            ], [
                'name' => 'show_images',
                'title' => 'admin::app.admin.system.show-images',
                'type' => 'boolean',
            ], [
                'name' => 'show_reviews',
                'title' => 'admin::app.admin.system.show-reviews',
                'type' => 'boolean',
            ], [
                'name' => 'show_ratings',
                'title' => 'admin::app.admin.system.show-ratings',
                'type' => 'boolean',
            ], [
                'name' => 'show_offers',
                'title' => 'admin::app.admin.system.show-offers',
                'type' => 'boolean',
            ],
        ],
    ], [
        'key' => 'catalog.rich_snippets.categories',
        'name' => 'admin::app.admin.system.categories',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'enable',
                'title' => 'admin::app.admin.system.enable',
                'type' => 'boolean',
            ], [
                'name' => 'show_search_input_field',
                'title' => 'admin::app.admin.system.show-search-input-field',
                'type' => 'boolean',
            ]
        ]
    ], [
        'key' => 'customer',
        'name' => 'admin::app.admin.system.customer',
        'sort' => 4,
    ], [
        'key' => 'customer.settings',
        'name' => 'admin::app.admin.system.settings',
        'sort' => 1,
    ], [
        'key' => 'customer.settings.address',
        'name' => 'admin::app.admin.system.address',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'street_lines',
                'title' => 'admin::app.admin.system.street-lines',
                'type' => 'text',
                'validation' => 'between:1,4',
                'channel_based' => true,
            ],
        ],
    ], [
        'key' => 'customer.settings.newsletter',
        'name' => 'admin::app.admin.system.newsletter',
        'sort' => 2,
        'fields' => [
            [
                'name' => 'subscription',
                'title' => 'admin::app.admin.system.newsletter-subscription',
                'type' => 'boolean',
            ],
        ],
    ], [
        'key' => 'customer.settings.email',
        'name' => 'admin::app.admin.system.email',
        'sort' => 3,
        'fields' => [
            [
                'name' => 'verification',
                'title' => 'admin::app.admin.system.email-verification',
                'type' => 'boolean',
            ],
        ],
    ], [
        'key' => 'emails',
        'name' => 'admin::app.admin.emails.email',
        'sort' => 5,
    ], [
        'key' => 'emails.general',
        'name' => 'admin::app.admin.emails.notification_label',
        'sort' => 1,
    ], [
        'key' => 'emails.general.notifications',
        'name' => 'admin::app.admin.emails.notification_label',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'emails.general.notifications.verification',
                'title' => 'admin::app.admin.emails.notifications.verification',
                'type' => 'boolean',
            ],
            [
                'name' => 'emails.general.notifications.registration',
                'title' => 'admin::app.admin.emails.notifications.registration',
                'type' => 'boolean',
            ],
            [
                'name' => 'emails.general.notifications.customer',
                'title' => 'admin::app.admin.emails.notifications.customer',
                'type' => 'boolean',
            ],
            [
                'name' => 'emails.general.notifications.new-order',
                'title' => 'admin::app.admin.emails.notifications.new-order',
                'type' => 'boolean',
            ],
            [
                'name' => 'emails.general.notifications.new-admin',
                'title' => 'admin::app.admin.emails.notifications.new-admin',
                'type' => 'boolean',
            ],
            [
                'name' => 'emails.general.notifications.new-invoice',
                'title' => 'admin::app.admin.emails.notifications.new-invoice',
                'type' => 'boolean',
            ],
            [
                'name' => 'emails.general.notifications.new-refund',
                'title' => 'admin::app.admin.emails.notifications.new-refund',
                'type' => 'boolean',
            ],
            [
                'name' => 'emails.general.notifications.new-shipment',
                'title' => 'admin::app.admin.emails.notifications.new-shipment',
                'type' => 'boolean',
            ],
            [
                'name' => 'emails.general.notifications.new-inventory-source',
                'title' => 'admin::app.admin.emails.notifications.new-inventory-source',
                'type' => 'boolean',
            ],
            [
                'name' => 'emails.general.notifications.cancel-order',
                'title' => 'admin::app.admin.emails.notifications.cancel-order',
                'type' => 'boolean',
            ],
        ],
    ],
    [
        'key' => 'loyalty',
        'name' => 'Loyalty',
        'sort' => 6,
    ], [
        'key' => 'loyalty.settings',
        'name' => 'Loyalty Settings',
        'sort' => 1,
    ],
    [
        'key' => 'loyalty.settings.purchase',
        'name' => 'Set User Purchase commission percentage',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'level_one_user_purchase',
                'title' => 'Level 01 User Purchase %',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'level_two_user_purchase',
                'title' => 'Level 02 User Purchase %',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'level_three_user_purchase',
                'title' => 'Level 03 User Purchase %',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'level_four_user_purchase',
                'title' => 'Level 04 User Purchase %',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ],
        ],
    ],
    [
        'key' => 'loyalty.settings.purchaselimit',
        'name' => 'Set User Redeem Limit',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'user_redeem_limit',
                'title' => 'User Redeem Limit',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ]
        ],
    ],
    [
        'key' => 'loyalty.settings.servicepoints',
        'name' => 'Set Service Points Deduction',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'service_points_deduction_amount',
                'title' => 'Service Points Deduction Amount',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'service_points_deduction_period',
                'title' => 'Service Points Deduction Period',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ]
        ],
    ],
    [
        'key' => 'loyalty.settings.signup',
        'name' => 'Set User SignUp Points',
        'sort' => 3,
        'fields' => [
            [
                'name' => 'level_one_user_ref',
                'title' => 'Level 01 User Referral Points',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'level_two_user_ref',
                'title' => 'Level 02 User Referral Points',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'level_three_user_ref',
                'title' => 'Level 03 User Referral Points',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'0',
                'ref'=>'loyalty'
            ],
        ],
    ],
    [
        'key' => 'delivery_location',
        'name' => 'Delivery Location',
        'sort' => 7,
    ], [
        'key' => 'delivery_location.add',
        'name' => 'Delivery Location',
        'sort' => 1,
    ],
    [
        'key' => 'delivery_location.add.add',
        'name' => 'Add Location City',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'district_id',
                'title' => 'District',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Ampara',
                        'value' => '1',
                    ], [
                        'title' => 'Anuradhapura',
                        'value' => '2',
                    ],
                    [
                        'title' => 'Badulla',
                        'value' => '3',
                    ],
                    [
                        'title' => 'Batticaloa',
                        'value' => '4',
                    ],
                    [
                        'title' => 'Colombo',
                        'value' => '5',
                    ],
                    [
                        'title' => 'Galle',
                        'value' => '6',
                    ],
                    [
                        'title' => 'Gampaha',
                        'value' => '7',
                    ],
                    [
                        'title' => 'Hambantota',
                        'value' => '8',
                    ],
                    [
                        'title' => 'Jaffna',
                        'value' => '9',
                    ],
                    [
                        'title' => 'Kalutara',
                        'value' => '10',
                    ],
                    [
                        'title' => 'Kandy',
                        'value' => '11',
                    ],
                    [
                        'title' => 'Kegalle',
                        'value' => '12',
                    ],
                    [
                        'title' => 'Kilinochchi',
                        'value' => '13',
                    ],
                    [
                        'title' => 'Kurunegala',
                        'value' => '14',
                    ],[
                        'title' => 'Mannar',
                        'value' => '15',
                    ],[
                        'title' => 'Matale',
                        'value' => '16',
                    ],[
                        'title' => 'Matara',
                        'value' => '17',
                    ],[
                        'title' => 'Moneragala',
                        'value' => '18',
                    ],[
                        'title' => 'Mullaitivu',
                        'value' => '19',
                    ],[
                        'title' => 'Nuwara Eliya',
                        'value' => '20',
                    ],[
                        'title' => 'Polonnaruwa',
                        'value' => '21',
                    ],[
                        'title' => 'Puttalam',
                        'value' => '22',
                    ],[
                        'title' => 'Ratnapura',
                        'value' => '23',
                    ],[
                        'title' => 'Trincomalee',
                        'value' => '24',
                    ],[
                        'title' => 'Vavuniya',
                        'value' => '25',
                    ],
                ],
                'channel_based' => false,
                'validation' => 'required'
            ],
            [
                'name' => 'city_name',
                'title' => 'City Name',
                'type' => 'text',
                'validation' => 'required',
                'default_value'=>'',
                'ref'=>'loyalty'
            ]
        ],
    ],[
        'key' => 'delivery_location.group',
        'name' => 'Delivery Location Groups',
        'sort' => 1,
    ],[
        'key' => 'delivery_location.group.add',
        'name' => 'Add Delivery Location Group',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'name',
                'title' => 'Name',
                'type' => 'text',
                'validation' => 'required',
                'default_value'=>'',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'delivery_charge',
                'title' => 'Delivery Charge',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'additional_kg',
                'title' => 'Additional Charge for KG',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'currency',
                'title' => 'Currency',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Rs.',
                        'value' => 'Rs.',
                    ]
                ],
                'channel_based' => false,
                'validation' => 'required'
            ],
            [
                'name' => 'dilivery_days',
                'title' => 'Delivery Days',
                'type' => 'select',
                'options' => [
                    [
                        'title' => '1-2',
                        'value' => '1-2',
                    ],[
                        'title' => '2-3',
                        'value' => '2-3',
                    ],[
                        'title' => '3-4',
                        'value' => '3-4',
                    ],[
                        'title' => '4-5',
                        'value' => '4-5',
                    ],[
                        'title' => '5-7',
                        'value' => '5-7',
                    ],[
                        'title' => '7-10',
                        'value' => '7-10',
                    ],[
                        'title' => '10-15',
                        'value' => '10-15',
                    ],[
                        'title' => '15-30',
                        'value' => '15-30',
                    ],
                ],
                'channel_based' => false,
                'validation' => 'required'
            ],
            [
                'name' => 'delivery_days_unit',
                'title' => 'Delivery Duration Unit',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Days',
                        'value' => 'Days',
                    ],
                    [
                        'title' => 'Hours',
                        'value' => 'Hours',
                    ],

                ],
                'channel_based' => false,
                'validation' => 'required'
            ],
            [
                'name' => 'is_cod_available',
                'title' => 'Is Cash On Delivery Available?',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Yes',
                        'value' => '1',
                    ],
                    [
                        'title' => 'No',
                        'value' => '0',
                    ],

                ],
                'channel_based' => false,
                'validation' => 'required'
            ],
        ],
    ],

    [
        'key' => 'delivery_location.assign',
        'name' => 'Delivery Location Groups Assign',
        'sort' => 1,
    ],[
        'key' => 'delivery_location.assign.add',
        'name' => 'Add Delivery Location Group',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'location_city_id',
                'title' => 'Location City IDs',
                'type' => 'multiselect',
                'options' => [
                    [
                        'title' => '1-2',
                        'value' => '1-2',
                    ]
                ],
                'channel_based' => false,
                'validation' => 'required',
            ],
            [
                'name' => 'delivery_charge',
                'title' => 'Delivery Charge',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'',
                'ref'=>'loyalty'
            ],
            [
                'name' => 'additional_kg',
                'title' => 'Additional Charge for KG',
                'type' => 'number',
                'validation' => 'required',
                'default_value'=>'',
                'ref'=>'loyalty'
            ],

            [
                'name' => 'dilivery_days',
                'title' => 'Delivery Days',
                'type' => 'select',
                'options' => [
                    [
                        'title' => '1-2',
                        'value' => '1-2',
                    ],[
                        'title' => '2-3',
                        'value' => '2-3',
                    ],[
                        'title' => '3-4',
                        'value' => '3-4',
                    ],[
                        'title' => '4-5',
                        'value' => '4-5',
                    ],[
                        'title' => '5-7',
                        'value' => '5-7',
                    ],[
                        'title' => '7-10',
                        'value' => '7-10',
                    ],[
                        'title' => '10-15',
                        'value' => '10-15',
                    ],[
                        'title' => '15-30',
                        'value' => '15-30',
                    ],
                ],
                'channel_based' => false,
                'validation' => 'required'
            ],
            [
                'name' => 'delivery_days_unit',
                'title' => 'Delivery Duration Unit',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Days',
                        'value' => 'Days',
                    ],
                    [
                        'title' => 'Hours',
                        'value' => 'Hours',
                    ],

                ],
                'channel_based' => false,
                'validation' => 'required'
            ],
            [
                'name' => 'is_cod_available',
                'title' => 'Is Cash On Delivery Available?',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Yes',
                        'value' => '1',
                    ],
                    [
                        'title' => 'No',
                        'value' => '0',
                    ],

                ],
                'channel_based' => false,
                'validation' => 'required'
            ],
        ],
    ],
];