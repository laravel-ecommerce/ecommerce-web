<div class="navbar-left">
    <ul class="menubar">
{{--        {{dd($menu->items)}}--}}
        @foreach ($menu->items as $menuItem)
            @if($menuItem['key']!= 'marketing')
                <li class="menu-item {{ $menu->getActive($menuItem) }}">
                    <a href="{{ count($menuItem['children']) ? current($menuItem['children'])['url'] : $menuItem['url'] }}">
                        <span class="icon {{ $menuItem['icon-class'] }}"></span>

                        <span>{{ $menuItem['name'] == 'velocity::app.admin.layouts.velocity' ? 'Ceibee' : trans($menuItem['name']) }}</span>
                    </a>
                </li>
            @endif
        @endforeach
    </ul>
</div>