@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.sales.orders.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('admin::app.sales.orders.title') }}</h1>
            </div>

            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span>
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="filtered-tags top-nav">
                <a href="{{route('admin.sales.orders.index')}}">
                    <span class="filter-tag" style="text-transform: capitalize;">
                        <span class="wrapper">
                                All
                        </span>
                    </span>
                </a>
                <a href="{{route('admin.sales.orders.index')}}?status[like]=pending">
                    <span class="filter-tag" style="text-transform: capitalize;">
                        <span class="wrapper">
                                pending
                        </span>
                    </span>
                </a>
                <a href="{{route('admin.sales.orders.index')}}?status[like]=processing">
                    <span class="filter-tag" style="text-transform: capitalize;">
                        <span class="wrapper">
                                Processing
                        </span>
                    </span>
                </a>
                <a href="{{route('admin.sales.orders.index')}}?status[like]=completed">
                    <span class="filter-tag" style="text-transform: capitalize;">
                        <span class="wrapper">
                                Shipped
                        </span>
                    </span>
                </a>
                <a href="{{route('admin.sales.orders.index')}}?status[like]=delivered">
                    <span class="filter-tag" style="text-transform: capitalize;">
                        <span class="wrapper">
                                Delivered
                        </span>
                    </span>
                </a>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="control-group" style="margin-bottom: 0px !important;">
                        <select name="" id="bulk_action" class="control">
                            <option value="">Select Bulk Action</option>
                            <option value="delivered">Mark As delivered</option>
                        </select>
                    </div>
                </div>
            </div>
            @inject('orderGrid', 'Webkul\Admin\DataGrids\OrderDataGrid')
            {!! $orderGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>

@stop

@push('scripts')
    @include('admin::export.export', ['gridName' => $orderGrid])

    <script>
        $(document).ready(function () {
            $("#bulk_action").change(function () {

                let selectedValue = $("#bulk_action").val();
                if (selectedValue != '') {
                    if (selectedValue == 'delivered') {
                        var bulkIDS = [];
                        $.each($("input[name='bulk_ids']:checked"), function () {
                            bulkIDS.push($(this).val());
                        });
                        if (bulkIDS.length > 0) {
                            let token = "{{session('_token')}}";
                            $.ajax({
                                type: "POST",
                                url: "{{ route('admin.sales.bulk_actions.proceed') }}",
                                data: {'_token': token, ids: bulkIDS, action: 'delivered'},
                                success: function (res) {
                                    if(res.status){
                                        alert('Records Updated');
                                        window.location.reload();
                                    }
                                }
                            });

                        }
                        console.log(bulkIDS);
                    }

                }
            });
        });
    </script>
@endpush