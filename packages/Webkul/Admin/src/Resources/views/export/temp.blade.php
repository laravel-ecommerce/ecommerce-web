<table>
    <thead>
    <tr>
        <th colspan="{{count($columns)}}" style="text-align: center"><b>Order Summary (Order vise count)</b></th>
    </tr>
    </thead>

</table>
<table>
    <thead>
    <tr>
        @foreach ($columns as $key => $value)
            <?php
            $title = $value == 'increment_id' ? 'order_id' : $value;
            ?>
            <th><b>{{ ucfirst(str_replace('_',' ', $title)) }}</b></th>
        @endforeach
    </tr>
    </thead>
    <tbody>

    @foreach ($records as $record)
        <tr>
            @foreach($record as $column => $value)
                <td>{{ $value }} </td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
@php
    $result = array();
    foreach ($records as $element) {
    $element = (array)$element;
        $data = ['total_qty_ordered'=>$element['total_qty_ordered'], 'sku'=>$element['sku'], 'created_at'=>$element['created_at']];
        $result[$element['name']][] = $data;
    }
    $final= [];
    foreach ($result as $key => $res) {
    $total_items = 0;
    $sku = '';
    $created_at = '';
        foreach ($res as $item) {
            $total_items += $item['total_qty_ordered'];
            $sku = $item['sku'];
            $created_at = \Carbon\Carbon::parse($item['created_at'])->format('Y-m-d');
        }
        $final[] = array('Product Name'=>$key, 'SKU'=>$sku, 'Total Items'=>$total_items , 'ordered At'=>$created_at);
    }
@endphp
<table>
    <thead>
    <tr>
        <th colspan="4" style="text-align: center"><b>Items order summary (Product vise count)</b></th>
    </tr>
    </thead>

</table>
<table>
    <thead>
    <tr>
        <th><b>Product Name</b></th>
        <th><b>SKU</b></th>
        <th><b>Total Items</b></th>
        <th><b>Ordered At</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach ($final as $record)
        <tr>
            @foreach($record as $column => $value)
                <td>{{ $value }} </td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>