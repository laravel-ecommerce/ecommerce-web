<?php


namespace Webkul\Core\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Webkul\Customer\Models\Customer;
use Webkul\Core\Contracts\Address as AddressContract;

class Province extends Model
{
    protected $table = 'location_provinces';

    protected $guarded =[];
    public $timestamps = false;

}
