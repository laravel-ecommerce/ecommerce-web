<?php


namespace Webkul\Core\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Webkul\Customer\Models\Customer;
use Webkul\Core\Contracts\Address as AddressContract;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'label',
        'index_key',
        'description',
        'value',
        'status'
    ];
    public $timestamps = false;

}
