<?php


namespace Webkul\Core\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Webkul\Customer\Models\Customer;
use Webkul\Core\Contracts\Address as AddressContract;

class LocationCitesGroup extends Model
{
    protected $table = 'location_cities_groups';

    protected $guarded =[];
    public $timestamps = false;

    public function location_groups()
    {
        return $this->hasOne(LocationGroup::class, 'id','location_cities_group_id');
    }

}
