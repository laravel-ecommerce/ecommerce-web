<?php

namespace Webkul\Customer\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Webkul\Checkout\Models\CartProxy;
use Webkul\Sales\Models\OrderProxy;
use Webkul\Core\Models\SubscribersListProxy;
use Webkul\Product\Models\ProductReviewProxy;
use Webkul\Customer\Notifications\CustomerResetPassword;
use Webkul\Customer\Contracts\Customer as CustomerContract;

class CustomerRef extends Model
{
    protected $table = 'customer_ref';

    protected $fillable = [
        'user_id',
        'referee_id',
        'level',

    ];

    public $timestamps = false;

    public function referee()
    {
        return $this->hasOne(Customer::class, 'id','referee_id');
    }

    public function refereer()
    {
        return $this->hasOne(Customer::class, 'id','user_id');
    }
}
