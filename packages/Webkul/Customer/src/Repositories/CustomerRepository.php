<?php

namespace Webkul\Customer\Repositories;

use Carbon\Carbon;
use Webkul\Core\Eloquent\Repository;

class CustomerRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */

    function model()
    {
        return 'Webkul\Customer\Contracts\Customer';
    }

    /**
     * Check if customer has order pending or processing.
     *
     * @param Webkul\Customer\Models\Customer
     * @return boolean
     */
    public function checkIfCustomerHasOrderPendingOrProcessing($customer)
    {
        return $customer->all_orders->pluck('status')->contains(function ($val) {
            return $val === 'pending' || $val === 'processing';
        });
    }

    /**
     * Check if bulk customers, if they have order pending or processing.
     *
     * @param array
     * @return boolean
     */
    public function checkBulkCustomerIfTheyHaveOrderPendingOrProcessing($customerIds)
    {
        foreach ($customerIds as $customerId) {
            $customer = $this->findorFail($customerId);

            if ($this->checkIfCustomerHasOrderPendingOrProcessing($customer)) {
                return true;
            }
        }

        return false;
    }

    public function updateAllOldCustomersPoints($points, $days)
    {
        $customers = $this->whereDate('last_points_deduct_date', Carbon::now()->subDays($days)->format('Y-m-d'))->get();
        $count = 0;
        foreach ($customers as $customer) {
            $customer_points = (int)($customer->points) - $points;
            $customer->points = ($customer_points > 0) ? $customer_points : 0;
            $customer->last_points_deduct_date = Carbon::now()->format('Y-m-d');
            $customer->save();
            $count++;
        }

        return $count;
    }
}