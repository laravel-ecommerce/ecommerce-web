<?php

namespace Webkul\Shop\Http\Controllers;
use Webkul\Customer\Models\Customer;
use Illuminate\Support\Str;

class InviteController extends Controller
{

    public function __construct()
    {

       parent::__construct();
    }

    public function index()
    {
        if(auth()->guard('customer')->user() == null){
            return redirect('/customer/login');
        }
        $ref_token = auth()->guard('customer')->user()->ref_token;

        if(empty($ref_token)){
            $usre_id = auth()->guard('customer')->user()->id;
            $token = $ref_token = Str::random(15);

            $customer = Customer::where('id', $usre_id)->update(['ref_token'=>$token]);
        }

        return $ref_token;
    }

    public function acceptInvitation($token)
    {
        return redirect('/customer/register?ref='.$token);
    }
}
