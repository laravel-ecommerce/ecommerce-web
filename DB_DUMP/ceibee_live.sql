-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2021 at 02:19 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ceibee_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `location_cities`
--

CREATE TABLE `location_cities` (
  `id` int(11) UNSIGNED NOT NULL,
  `district_id` int(11) NOT NULL,
  `city_name` varchar(20) NOT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `city_code` varchar(5) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `popular_status` enum('yes','no') NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_cities`
--

INSERT INTO `location_cities` (`id`, `district_id`, `city_name`, `slug`, `city_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`, `popular_status`) VALUES
(1, 5, 'Nugegoda', 'nugegoda', 'nuge', 1, 1, '2017-07-08 15:09:03', '2017-07-09 15:09:03', 'active', 'yes'),
(2, 5, 'Dehiwala', 'dehiwala', 'dehi', 1, 1, '2017-07-09 15:09:03', '2017-07-10 15:09:03', 'active', 'yes'),
(3, 5, 'Piliyandala', 'piliyandala', 'pili', 1, 1, '2017-07-10 15:09:03', '2017-07-11 15:09:03', 'active', 'yes'),
(4, 5, 'Maharagama', 'maharagama', 'maha', 1, 1, '2017-07-11 15:09:03', '2017-07-12 15:09:03', 'active', 'yes'),
(5, 5, 'Angoda', 'angoda', 'ango', 1, 1, '2017-07-12 15:09:03', '2017-07-13 15:09:03', 'active', 'yes'),
(6, 5, 'Athurugiriya', 'athurugiriya', 'athu', 1, 1, '2017-07-13 15:09:03', '2017-07-14 15:09:03', 'active', 'yes'),
(7, 5, 'Avissawella', 'avissawella', 'avis', 1, 1, '2017-07-14 15:09:03', '2017-07-15 15:09:03', 'active', 'yes'),
(8, 5, 'Battaramulla', 'battaramulla', 'batt', 1, 1, '2017-07-15 15:09:03', '2017-07-16 15:09:03', 'active', 'yes'),
(9, 5, 'Boralesgamuwa', 'boralesgamuwa', 'bora', 1, 1, '2017-07-16 15:09:03', '2017-07-17 15:09:03', 'active', 'yes'),
(10, 5, 'Colombo 1', 'colombo-1', 'col1', 1, 1, '2017-07-17 15:09:03', '2017-07-18 15:09:03', 'active', 'yes'),
(11, 5, 'Colombo 2', 'colombo-2', 'col2', 1, 1, '2017-07-18 15:09:03', '2017-07-19 15:09:03', 'active', 'yes'),
(12, 5, 'Colombo 3', 'colombo-3', 'col3', 1, 1, '2017-07-19 15:09:03', '2017-07-20 15:09:03', 'active', 'yes'),
(13, 5, 'Colombo 4', 'colombo-4', 'col4', 1, 1, '2017-07-20 15:09:03', '2017-07-21 15:09:03', 'active', 'yes'),
(14, 5, 'Colombo 5', 'colombo-5', 'col5', 1, 1, '2017-07-21 15:09:03', '2017-07-22 15:09:03', 'active', 'yes'),
(15, 5, 'Colombo 6', 'colombo-6', 'col6', 1, 1, '2017-07-22 15:09:03', '2017-07-23 15:09:03', 'active', 'yes'),
(16, 5, 'Colombo 7', 'colombo-7', 'col7', 1, 1, '2017-07-23 15:09:03', '2017-07-24 15:09:03', 'active', 'yes'),
(17, 5, 'Colombo 8', 'colombo-8', 'col8', 1, 1, '2017-07-24 15:09:03', '2017-07-25 15:09:03', 'active', 'yes'),
(18, 5, 'Colombo 9', 'colombo-9', 'col9', 1, 1, '2017-07-25 15:09:03', '2017-07-26 15:09:03', 'active', 'yes'),
(19, 5, 'Colombo 10', 'colombo-10', 'col10', 1, 1, '2017-07-26 15:09:03', '2017-07-27 15:09:03', 'active', 'yes'),
(20, 5, 'Colombo 11', 'colombo-11', 'col11', 1, 1, '2017-07-27 15:09:03', '2017-07-28 15:09:03', 'active', 'yes'),
(21, 5, 'Colombo 12', 'colombo-12', 'col12', 1, 1, '2017-07-28 15:09:03', '2017-07-29 15:09:03', 'active', 'yes'),
(22, 5, 'Colombo 13', 'colombo-13', 'col13', 1, 1, '2017-07-29 15:09:03', '2017-07-30 15:09:03', 'active', 'yes'),
(23, 5, 'Colombo 14', 'colombo-14', 'col14', 1, 1, '2017-07-30 15:09:03', '2017-07-31 15:09:03', 'active', 'yes'),
(24, 5, 'Colombo 15', 'colombo-15', 'col15', 1, 1, '2017-07-31 15:09:03', '2017-08-01 15:09:03', 'active', 'yes'),
(25, 5, 'Hanwella', 'hanwella', 'han', 1, 1, '2017-08-01 15:09:03', '2017-08-02 15:09:03', 'active', 'yes'),
(26, 5, 'Homagama', 'homagama', 'homa', 1, 1, '2017-08-02 15:09:03', '2017-08-03 15:09:03', 'active', 'yes'),
(27, 5, 'Kaduwela', 'kaduwela', 'kadu', 1, 1, '2017-08-03 15:09:03', '2017-08-04 15:09:03', 'active', 'yes'),
(28, 5, 'Kesbewa', 'kesbewa', 'kesb', 1, 1, '2017-08-04 15:09:03', '2017-08-05 15:09:03', 'active', 'yes'),
(29, 5, 'Kohuwala', 'kohuwala', 'kohu', 1, 1, '2017-08-05 15:09:03', '2017-08-06 15:09:03', 'active', 'yes'),
(30, 5, 'Kolonnawa', 'kolonnawa', 'kolo', 1, 1, '2017-08-06 15:09:03', '2017-08-07 15:09:03', 'active', 'yes'),
(31, 5, 'Kottawa', 'kottawa', 'kotta', 1, 1, '2017-08-07 15:09:03', '2017-08-08 15:09:03', 'active', 'yes'),
(32, 5, 'Kotte', 'kotte', 'kotte', 1, 1, '2017-08-08 15:09:03', '2017-08-09 15:09:03', 'active', 'yes'),
(33, 5, 'Malabe', 'malabe', 'mala', 1, 1, '2017-08-09 15:09:03', '2017-08-10 15:09:03', 'active', 'yes'),
(34, 5, 'Moratuwa', 'moratuwa', 'mora', 1, 1, '2017-08-10 15:09:03', '2017-08-11 15:09:03', 'active', 'yes'),
(35, 5, 'Mount Lavinia', 'mount-lavinia', 'mtlav', 1, 1, '2017-08-11 15:09:03', '2017-08-12 15:09:03', 'active', 'yes'),
(36, 5, 'Nawala', 'nawala', 'nawa', 1, 1, '2017-08-12 15:09:03', '2017-08-13 15:09:03', 'active', 'yes'),
(37, 5, 'Padukka', 'padukka', 'paduk', 1, 1, '2017-08-13 15:09:03', '2017-08-14 15:09:03', 'active', 'yes'),
(38, 5, 'Pannipitiya', 'pannipitiya', 'panni', 1, 1, '2017-08-14 15:09:03', '2017-08-15 15:09:03', 'active', 'yes'),
(39, 5, 'Rajagiriya', 'rajagiriya', 'raja', 1, 1, '2017-08-15 15:09:03', '2017-08-16 15:09:03', 'active', 'yes'),
(40, 5, 'Ratmalana', 'ratmalana', 'ratma', 1, 1, '2017-08-16 15:09:03', '2017-08-17 15:09:03', 'active', 'yes'),
(41, 5, 'Talawatugoda', 'talawatugoda', 'talaw', 1, 1, '2017-08-17 15:09:03', '2017-08-18 15:09:03', 'active', 'yes'),
(42, 5, 'Wellampitiya', 'wellampitiya', 'wella', 1, 1, '2017-08-18 15:09:03', '2017-08-19 15:09:03', 'active', 'yes'),
(43, 11, 'Kandy', 'kandy', 'kandy', 1, 1, '2017-08-19 15:09:03', '2017-08-20 15:09:03', 'active', 'yes'),
(44, 11, 'Katugastota', 'katugastota', 'katug', 1, 1, '2017-08-20 15:09:03', '2017-08-21 15:09:03', 'active', 'yes'),
(45, 11, 'Gampola', 'gampola', 'gamp', 1, 1, '2017-08-21 15:09:03', '2017-08-22 15:09:03', 'active', 'yes'),
(46, 11, 'Kundasale', 'kundasale', 'kund', 1, 1, '2017-08-22 15:09:03', '2017-08-23 15:09:03', 'active', 'yes'),
(47, 11, 'Peradeniya', 'peradeniya', 'pera', 1, 1, '2017-08-23 15:09:03', '2017-08-24 15:09:03', 'active', 'yes'),
(48, 11, 'Akurana', 'akurana', 'aku', 1, 1, '2017-08-24 15:09:03', '2017-08-25 15:09:03', 'active', 'yes'),
(49, 11, 'Ampitiya', 'ampitiya', 'ampi', 1, 1, '2017-08-25 15:09:03', '2017-08-26 15:09:03', 'active', 'yes'),
(50, 11, 'Digana', 'digana', 'digan', 1, 1, '2017-08-26 15:09:03', '2017-08-27 15:09:03', 'active', 'yes'),
(51, 11, 'Galagedara', 'galagedara', 'galag', 1, 1, '2017-08-27 15:09:03', '2017-08-28 15:09:03', 'active', 'yes'),
(52, 11, 'Gelioya', 'gelioya', 'geli', 1, 1, '2017-08-28 15:09:03', '2017-08-29 15:09:03', 'active', 'yes'),
(53, 11, 'Kadugannawa', 'kadugannawa', 'kadug', 1, 1, '2017-08-29 15:09:03', '2017-08-30 15:09:03', 'active', 'yes'),
(54, 11, 'Madawala Bazaar', 'madawala-bazaar', 'madaw', 1, 1, '2017-08-30 15:09:03', '2017-08-31 15:09:03', 'active', 'yes'),
(55, 11, 'Nawalapitiya', 'nawalapitiya', 'nawa', 1, 1, '2017-08-31 15:09:03', '2017-09-01 15:09:03', 'active', 'yes'),
(56, 11, 'Pilimatalawa', 'pilimatalawa', 'pilim', 1, 1, '2017-09-01 15:09:03', '2017-09-02 15:09:03', 'active', 'yes'),
(57, 11, 'Wattegama', 'wattegama', 'watt', 1, 1, '2017-09-02 15:09:03', '2017-09-03 15:09:03', 'active', 'yes'),
(58, 6, 'Galle', 'galle', 'gall', 1, 1, '2017-09-03 15:09:03', '2017-09-04 15:09:03', 'active', 'yes'),
(59, 6, 'Ambalangoda', 'ambalangoda', 'amba', 1, 1, '2017-09-04 15:09:03', '2017-09-05 15:09:03', 'active', 'yes'),
(60, 6, 'Elpitiya', 'elpitiya', 'elpi', 1, 1, '2017-09-05 15:09:03', '2017-09-06 15:09:03', 'active', 'yes'),
(61, 6, 'Hikkaduwa', 'hikkaduwa', 'hikk', 1, 1, '2017-09-06 15:09:03', '2017-09-07 15:09:03', 'active', 'yes'),
(62, 6, 'Baddegama', 'baddegama', 'badd', 1, 1, '2017-09-07 15:09:03', '2017-09-08 15:09:03', 'active', 'yes'),
(63, 6, 'Ahangama', 'ahangama', 'ahan', 1, 1, '2017-09-08 15:09:03', '2017-09-09 15:09:03', 'active', 'yes'),
(64, 6, 'Balapitiya', 'balapitiya', 'balap', 1, 1, '2017-09-09 15:09:03', '2017-09-10 15:09:03', 'active', 'yes'),
(65, 6, 'Benthota', 'benthota', 'bent', 1, 1, '2017-09-10 15:09:03', '2017-09-11 15:09:03', 'active', 'yes'),
(66, 6, 'Karapitiya', 'karapitiya', 'karap', 1, 1, '2017-09-11 15:09:03', '2017-09-12 15:09:03', 'active', 'yes'),
(67, 1, 'Ampara', 'ampara', 'ampa', 1, 1, '2017-09-12 15:09:03', '2017-09-13 15:09:03', 'active', 'yes'),
(68, 1, 'Akkarepattu', 'akkarepattu', 'akkar', 1, 1, '2017-09-13 15:09:03', '2017-09-14 15:09:03', 'active', 'yes'),
(69, 1, 'Kalmunai', 'kalmunai', 'kalm', 1, 1, '2017-09-14 15:09:03', '2017-09-15 15:09:03', 'active', 'yes'),
(70, 1, 'Sainthamaruthu', 'sainthamaruthu', 'sain', 1, 1, '2017-09-15 15:09:03', '2017-09-16 15:09:03', 'active', 'yes'),
(71, 2, 'Anuradhapura', 'anuradhapura', 'anur', 1, 1, '2017-09-16 15:09:03', '2017-09-17 15:09:03', 'active', 'yes'),
(72, 2, 'Kekirawa', 'kekirawa', 'keki', 1, 1, '2017-09-17 15:09:03', '2017-09-18 15:09:03', 'active', 'yes'),
(73, 2, 'Tambuttegama', 'tambuttegama', 'tamb', 1, 1, '2017-09-18 15:09:03', '2017-09-19 15:09:03', 'active', 'yes'),
(74, 2, 'Medawachchiya', 'medawachchiya', 'medaw', 1, 1, '2017-09-19 15:09:03', '2017-09-20 15:09:03', 'active', 'yes'),
(75, 2, 'Eppawala', 'eppawala', 'eppa', 1, 1, '2017-09-20 15:09:03', '2017-09-21 15:09:03', 'active', 'yes'),
(76, 2, 'Galenbindunuwewa', 'galenbindunuwewa', 'galen', 1, 1, '2017-09-21 15:09:03', '2017-09-22 15:09:03', 'active', 'yes'),
(77, 2, 'Galnewa', 'galnewa', 'galn', 1, 1, '2017-09-22 15:09:03', '2017-09-23 15:09:03', 'active', 'yes'),
(78, 2, 'Habarana', 'habarana', 'haba', 1, 1, '2017-09-23 15:09:03', '2017-09-24 15:09:03', 'active', 'yes'),
(79, 2, 'Mihintale', 'mihintale', 'mihi', 1, 1, '2017-09-24 15:09:03', '2017-09-25 15:09:03', 'active', 'yes'),
(80, 2, 'Nochchiyagama', 'nochchiyagama', 'noch', 1, 1, '2017-09-25 15:09:03', '2017-09-26 15:09:03', 'active', 'yes'),
(81, 2, 'Talawa', 'talawa', 'tala', 1, 1, '2017-09-26 15:09:03', '2017-09-27 15:09:03', 'active', 'yes'),
(82, 3, 'Badulla', 'badulla', 'badul', 1, 1, '2017-09-27 15:09:03', '2017-09-28 15:09:03', 'active', 'yes'),
(83, 3, 'Bandarawela', 'bandarawela', 'banda', 1, 1, '2017-09-28 15:09:03', '2017-09-29 15:09:03', 'active', 'yes'),
(84, 3, 'Welimada', 'welimada', 'weli', 1, 1, '2017-09-29 15:09:03', '2017-09-30 15:09:03', 'active', 'yes'),
(85, 3, 'Mahiyanganaya', 'mahiyanganaya', 'mahi', 1, 1, '2017-09-30 15:09:03', '2017-10-01 15:09:03', 'active', 'yes'),
(86, 3, 'Hali Ela', 'hali-ela', 'hali', 1, 1, '2017-10-01 15:09:03', '2017-10-02 15:09:03', 'active', 'yes'),
(87, 3, 'Diyathalawa', 'diyathalawa', 'diya', 1, 1, '2017-10-02 15:09:03', '2017-10-03 15:09:03', 'active', 'yes'),
(88, 3, 'Ella', 'ella', 'ella', 1, 1, '2017-10-03 15:09:03', '2017-10-04 15:09:03', 'active', 'yes'),
(89, 3, 'Haputhale', 'haputhale', 'hapu', 1, 1, '2017-10-04 15:09:03', '2017-10-05 15:09:03', 'active', 'yes'),
(90, 3, 'Passara', 'passara', 'pass', 1, 1, '2017-10-05 15:09:03', '2017-10-06 15:09:03', 'active', 'yes'),
(91, 4, 'Batticaloa', 'batticaloa', 'bat', 1, 1, '2017-10-06 15:09:03', '2017-10-07 15:09:03', 'active', 'yes'),
(92, 7, 'Gampaha', 'gampaha', 'gam', 1, 1, '2017-10-07 15:09:03', '2017-10-08 15:09:03', 'active', 'yes'),
(93, 7, 'Negombo', 'negombo', 'nego', 1, 1, '2017-10-08 15:09:03', '2017-10-09 15:09:03', 'active', 'yes'),
(94, 7, 'Kelaniya', 'kelaniya', 'kela', 1, 1, '2017-10-09 15:09:03', '2017-10-10 15:09:03', 'active', 'yes'),
(95, 7, 'Kadawatha', 'kadawatha', 'kadaw', 1, 1, '2017-10-10 15:09:03', '2017-10-11 15:09:03', 'active', 'yes'),
(96, 7, 'Ja-Ela', 'ja-ela', 'jaela', 1, 1, '2017-10-11 15:09:03', '2017-10-12 15:09:03', 'active', 'yes'),
(97, 7, 'Delgoda', 'delgoda', 'delg', 1, 1, '2017-10-12 15:09:03', '2017-10-13 15:09:03', 'active', 'yes'),
(98, 7, 'Divulapitiya', 'divulapitiya', 'divu', 1, 1, '2017-10-13 15:09:03', '2017-10-14 15:09:03', 'active', 'yes'),
(99, 7, 'Ganemulla', 'ganemulla', 'gane', 1, 1, '2017-10-14 15:09:03', '2017-10-15 15:09:03', 'active', 'yes'),
(100, 7, 'Kandana', 'kandana', 'kada', 1, 1, '2017-10-15 15:09:03', '2017-10-16 15:09:03', 'active', 'yes'),
(101, 7, 'Katunayake', 'katunayake', 'katun', 1, 1, '2017-10-16 15:09:03', '2017-10-17 15:09:03', 'active', 'yes'),
(102, 7, 'Kiribathgoda', 'kiribathgoda', 'kirib', 1, 1, '2017-10-17 15:09:03', '2017-10-18 15:09:03', 'active', 'yes'),
(103, 7, 'Minuwangoda', 'minuwangoda', 'minuw', 1, 1, '2017-10-18 15:09:03', '2017-10-19 15:09:03', 'active', 'yes'),
(104, 7, 'Mirigama', 'mirigama', 'miri', 1, 1, '2017-10-19 15:09:03', '2017-10-20 15:09:03', 'active', 'yes'),
(105, 7, 'Nittambuwa', 'nittambuwa', 'nitt', 1, 1, '2017-10-20 15:09:03', '2017-10-21 15:09:03', 'active', 'yes'),
(106, 7, 'Ragama', 'ragama', 'raga', 1, 1, '2017-10-21 15:09:03', '2017-10-22 15:09:03', 'active', 'yes'),
(107, 7, 'Veyangoda', 'veyangoda', 'veya', 1, 1, '2017-10-22 15:09:03', '2017-10-23 15:09:03', 'active', 'yes'),
(108, 7, 'Wattala', 'wattala', 'watta', 1, 1, '2017-10-23 15:09:03', '2017-10-24 15:09:03', 'active', 'yes'),
(109, 8, 'Tangalla', 'tangalla', 'tang', 1, 1, '2017-10-24 15:09:03', '2017-10-25 15:09:03', 'active', 'yes'),
(110, 8, 'Beliatta', 'beliatta', 'beli', 1, 1, '2017-10-25 15:09:03', '2017-10-26 15:09:03', 'active', 'yes'),
(111, 8, 'Tissamaharama', 'tissamaharama', 'tiss', 1, 1, '2017-10-26 15:09:03', '2017-10-27 15:09:03', 'active', 'yes'),
(112, 8, 'Hambantota', 'hambantota', 'hamb', 1, 1, '2017-10-27 15:09:03', '2017-10-28 15:09:03', 'active', 'yes'),
(113, 8, 'Ambalantota', 'ambalantota', 'amb', 1, 1, '2017-10-28 15:09:03', '2017-10-29 16:09:03', 'active', 'yes'),
(114, 9, 'Jaffna', 'jaffna', 'jaff', 1, 1, '2017-10-29 16:09:03', '2017-10-30 16:09:03', 'active', 'yes'),
(115, 9, 'Nallur', 'nallur', 'nall', 1, 1, '2017-10-30 16:09:03', '2017-10-31 16:09:03', 'active', 'yes'),
(116, 9, 'Chavakachcheri', 'chavakachcheri', 'chav', 1, 1, '2017-10-31 16:09:03', '2017-11-01 16:09:03', 'active', 'yes'),
(117, 10, 'Horana', 'horana', 'hora', 1, 1, '2017-11-01 16:09:03', '2017-11-02 16:09:03', 'active', 'yes'),
(118, 10, 'Kalutara', 'kalutara', 'kalu', 1, 1, '2017-11-02 16:09:03', '2017-11-03 16:09:03', 'active', 'yes'),
(119, 10, 'Panadura', 'panadura', 'pana', 1, 1, '2017-11-03 16:09:03', '2017-11-04 16:09:03', 'active', 'yes'),
(120, 10, 'Bandaragama', 'bandaragama', 'band', 1, 1, '2017-11-04 16:09:03', '2017-11-05 15:09:03', 'active', 'yes'),
(121, 10, 'Matugama', 'matugama', 'matu', 1, 1, '2017-11-05 15:09:03', '2017-11-06 15:09:03', 'active', 'yes'),
(122, 10, 'Aluthgama', 'aluthgama', 'alut', 1, 1, '2017-11-06 15:09:03', '2017-11-07 15:09:03', 'active', 'yes'),
(123, 10, 'Beruwala', 'beruwala', 'beru', 1, 1, '2017-11-07 15:09:03', '2017-11-08 15:09:03', 'active', 'yes'),
(124, 10, 'Ingiriya', 'ingiriya', 'ingi', 1, 1, '2017-11-08 15:09:03', '2017-11-09 15:09:03', 'active', 'yes'),
(125, 10, 'Wadduwa', 'wadduwa', 'wadd', 1, 1, '2017-11-09 15:09:03', '2017-11-10 15:09:03', 'active', 'yes'),
(126, 12, 'Kegalle', 'kegalle', 'kega', 1, 1, '2017-11-10 15:09:03', '2017-11-11 15:09:03', 'active', 'yes'),
(127, 12, 'Mawanella', 'mawanella', 'mawa', 1, 1, '2017-11-11 15:09:03', '2017-11-12 15:09:03', 'active', 'yes'),
(128, 12, 'Warakapola', 'warakapola', 'wara', 1, 1, '2017-11-12 15:09:03', '2017-11-13 15:09:03', 'active', 'yes'),
(129, 12, 'Rambukkana', 'rambukkana', 'ramb', 1, 1, '2017-11-13 15:09:03', '2017-11-14 15:09:03', 'active', 'yes'),
(130, 12, 'Ruwanwella', 'ruwanwella', 'ruwan', 1, 1, '2017-11-14 15:09:03', '2017-11-15 15:09:03', 'active', 'yes'),
(131, 12, 'Dehiowita', 'dehiowita', 'dehio', 1, 1, '2017-11-15 15:09:03', '2017-11-16 15:09:03', 'active', 'yes'),
(132, 12, 'Deraniyagala', 'deraniyagala', 'dera', 1, 1, '2017-11-16 15:09:03', '2017-11-17 15:09:03', 'active', 'yes'),
(133, 12, 'Galigamuwa', 'galigamuwa', 'galig', 1, 1, '2017-11-17 15:09:03', '2017-11-18 15:09:03', 'active', 'yes'),
(134, 12, 'Kithulgala', 'kithulgala', 'kitu', 1, 1, '2017-11-18 15:09:03', '2017-11-19 15:09:03', 'active', 'yes'),
(135, 12, 'Yatiyanthota', 'yatiyanthota', 'yatiy', 1, 1, '2017-11-19 15:09:03', '2017-11-20 15:09:03', 'active', 'yes'),
(136, 13, 'Kilinochchi', 'kilinochchi', 'kilin', 1, 1, '2017-11-20 15:09:03', '2017-11-21 15:09:03', 'active', 'yes'),
(137, 14, 'Kurunegala', 'kurunegala', 'kuru', 1, 1, '2017-11-21 15:09:03', '2017-11-22 15:09:03', 'active', 'yes'),
(138, 14, 'Kuliyapitiya', 'kuliyapitiya', 'kuli', 1, 1, '2017-11-22 15:09:03', '2017-11-23 15:09:03', 'active', 'yes'),
(139, 14, 'Pannala', 'pannala', 'pann', 1, 1, '2017-11-23 15:09:03', '2017-11-24 15:09:03', 'active', 'yes'),
(140, 14, 'Narammala', 'narammala', 'naram', 1, 1, '2017-11-24 15:09:03', '2017-11-25 15:09:03', 'active', 'yes'),
(141, 14, 'Wariyapola', 'wariyapola', 'wariy', 1, 1, '2017-11-25 15:09:03', '2017-11-26 15:09:03', 'active', 'yes'),
(142, 14, 'Alawwa', 'alawwa', 'alaw', 1, 1, '2017-11-26 15:09:03', '2017-11-27 15:09:03', 'active', 'yes'),
(143, 14, 'Bingiriya', 'bingiriya', 'bing', 1, 1, '2017-11-27 15:09:03', '2017-11-28 15:09:03', 'active', 'yes'),
(144, 14, 'Galgamuwa', 'galgamuwa', 'galg', 1, 1, '2017-11-28 15:09:03', '2017-11-29 15:09:03', 'active', 'yes'),
(145, 14, 'Giriulla', 'giriulla', 'giriu', 1, 1, '2017-11-29 15:09:03', '2017-11-30 15:09:03', 'active', 'yes'),
(146, 14, 'Hettipola', 'hettipola', 'hett', 1, 1, '2017-11-30 15:09:03', '2017-12-01 15:09:03', 'active', 'yes'),
(147, 14, 'Ibbagamuwa', 'ibbagamuwa', 'ibba', 1, 1, '2017-12-01 15:09:03', '2017-12-02 15:09:03', 'active', 'yes'),
(148, 14, 'Mawathagama', 'mawathagama', 'mawat', 1, 1, '2017-12-02 15:09:03', '2017-12-03 15:09:03', 'active', 'yes'),
(149, 14, 'Nikaweratiya', 'nikaweratiya', 'nika', 1, 1, '2017-12-03 15:09:03', '2017-12-04 15:09:03', 'active', 'yes'),
(150, 14, 'Polgahawela', 'polgahawela', 'polg', 1, 1, '2017-12-04 15:09:03', '2017-12-05 15:09:03', 'active', 'yes'),
(151, 15, 'Mannar', 'mannar', 'mann', 1, 1, '2017-12-05 15:09:03', '2017-12-06 15:09:03', 'active', 'yes'),
(152, 16, 'Matale', 'matale', 'mata', 1, 1, '2017-12-06 15:09:03', '2017-12-07 15:09:03', 'active', 'yes'),
(153, 16, 'Dambulla', 'dambulla', 'damb', 1, 1, '2017-12-07 15:09:03', '2017-12-08 15:09:03', 'active', 'yes'),
(154, 16, 'Galewela', 'galewela', 'galew', 1, 1, '2017-12-08 15:09:03', '2017-12-09 15:09:03', 'active', 'yes'),
(155, 16, 'Ukuwela', 'ukuwela', 'ukuw', 1, 1, '2017-12-09 15:09:03', '2017-12-10 15:09:03', 'active', 'yes'),
(156, 16, 'Palapathwela', 'palapathwela', 'palap', 1, 1, '2017-12-10 15:09:03', '2017-12-11 15:09:03', 'active', 'yes'),
(157, 16, 'Rattota', 'rattota', 'ratt', 1, 1, '2017-12-11 15:09:03', '2017-12-12 15:09:03', 'active', 'yes'),
(158, 16, 'Sigiriya', 'sigiriya', 'sigir', 1, 1, '2017-12-12 15:09:03', '2017-12-13 15:09:03', 'active', 'yes'),
(159, 16, 'Yatawatta', 'yatawatta', 'yataw', 1, 1, '2017-12-13 15:09:03', '2017-12-14 15:09:03', 'active', 'yes'),
(160, 17, 'Matara', 'matara', 'matar', 1, 1, '2017-12-14 15:09:03', '2017-12-15 15:09:03', 'active', 'yes'),
(161, 17, 'Weligama', 'weligama', 'welig', 1, 1, '2017-12-15 15:09:03', '2017-12-16 15:09:03', 'active', 'yes'),
(162, 17, 'Akuressa', 'akuressa', 'akur', 1, 1, '2017-12-16 15:09:03', '2017-12-17 15:09:03', 'active', 'yes'),
(163, 17, 'Hakmana', 'hakmana', 'hakm', 1, 1, '2017-12-17 15:09:03', '2017-12-18 15:09:03', 'active', 'yes'),
(164, 17, 'Dikwella', 'dikwella', 'dikw', 1, 1, '2017-12-18 15:09:03', '2017-12-19 15:09:03', 'active', 'yes'),
(165, 17, 'Deniyaya', 'deniyaya', 'deniy', 1, 1, '2017-12-19 15:09:03', '2017-12-20 15:09:03', 'active', 'yes'),
(166, 17, 'Kamburugamuwa', 'kamburugamuwa', 'kamb', 1, 1, '2017-12-20 15:09:03', '2017-12-21 15:09:03', 'active', 'yes'),
(167, 17, 'Kamburupitya', 'kamburupitya', 'kambu', 1, 1, '2017-12-21 15:09:03', '2017-12-22 15:09:03', 'active', 'yes'),
(168, 17, 'Kekanadurra', 'kekanadurra', 'kekan', 1, 1, '2017-12-22 15:09:03', '2017-12-23 15:09:03', 'active', 'yes'),
(169, 18, 'Moneragala', 'moneragala', 'mone', 1, 1, '2017-12-23 15:09:03', '2017-12-24 15:09:03', 'active', 'yes'),
(170, 18, 'Wellawaya', 'wellawaya', 'well', 1, 1, '2017-12-24 15:09:03', '2017-12-25 15:09:03', 'active', 'yes'),
(171, 18, 'Bibile', 'bibile', 'bibi', 1, 1, '2017-12-25 15:09:03', '2017-12-26 15:09:03', 'active', 'yes'),
(172, 18, 'Buttala', 'buttala', 'butt', 1, 1, '2017-12-26 15:09:03', '2017-12-27 15:09:03', 'active', 'yes'),
(173, 18, 'Kataragama', 'kataragama', 'katar', 1, 1, '2017-12-27 15:09:03', '2017-12-28 15:09:03', 'active', 'yes'),
(174, 19, 'Mullativu', 'mullativu', 'mull', 1, 1, '2017-12-28 15:09:03', '2017-12-29 15:09:03', 'active', 'yes'),
(175, 20, 'Nuwara Eliya', 'nuwara-eliya', 'nuwa', 1, 1, '2017-12-29 15:09:03', '2017-12-30 15:09:03', 'active', 'yes'),
(176, 20, 'Hatton', 'hatton', 'hatt', 1, 1, '2017-12-30 15:09:03', '2017-12-31 15:09:03', 'active', 'yes'),
(177, 20, 'Ginigathena', 'ginigathena', 'gini', 1, 1, '2017-12-31 15:09:03', '2018-01-01 15:09:03', 'active', 'yes'),
(178, 20, 'Madulla', 'madulla', 'madul', 1, 1, '2018-01-01 15:09:03', '2018-01-02 15:09:03', 'active', 'yes'),
(179, 21, 'Polonnaruwa', 'polonnaruwa', 'polo', 1, 1, '2018-01-02 15:09:03', '2018-01-03 15:09:03', 'active', 'yes'),
(180, 21, 'Hingurakgoda', 'hingurakgoda', 'hing', 1, 1, '2018-01-03 15:09:03', '2018-01-04 15:09:03', 'active', 'yes'),
(181, 21, 'Kaduruwela', 'kaduruwela', 'kaduw', 1, 1, '2018-01-04 15:09:03', '2018-01-05 15:09:03', 'active', 'yes'),
(182, 21, 'Medirigiriya', 'medirigiriya', 'medir', 1, 1, '2018-01-05 15:09:03', '2018-01-06 15:09:03', 'active', 'yes'),
(183, 22, 'Chilaw', 'chilaw', 'chil', 1, 1, '2018-01-06 15:09:03', '2018-01-07 15:09:03', 'active', 'yes'),
(184, 22, 'Wennappuwa', 'wennappuwa', 'wenn', 1, 1, '2018-01-07 15:09:03', '2018-01-08 15:09:03', 'active', 'yes'),
(185, 22, 'Puttalam', 'puttalam', 'putt', 1, 1, '2018-01-08 15:09:03', '2018-01-09 15:09:03', 'active', 'yes'),
(186, 22, 'Dankotuwa', 'dankotuwa', 'dank', 1, 1, '2018-01-09 15:09:03', '2018-01-10 15:09:03', 'active', 'yes'),
(187, 22, 'Nattandiya', 'nattandiya', 'natt', 1, 1, '2018-01-10 15:09:03', '2018-01-11 15:09:03', 'active', 'yes'),
(188, 22, 'Marawila', 'marawila', 'mara', 1, 1, '2018-01-11 15:09:03', '2018-01-12 15:09:03', 'active', 'yes'),
(189, 23, 'Ratnapura', 'ratnapura', 'ratn', 1, 1, '2018-01-12 15:09:03', '2018-01-13 15:09:03', 'active', 'yes'),
(190, 23, 'Embilipitiya', 'embilipitiya', 'embi', 1, 1, '2018-01-13 15:09:03', '2018-01-14 15:09:03', 'active', 'yes'),
(191, 23, 'Balangoda', 'balangoda', 'balan', 1, 1, '2018-01-14 15:09:03', '2018-01-15 15:09:03', 'active', 'yes'),
(192, 23, 'Pelmadulla', 'pelmadulla', 'pelm', 1, 1, '2018-01-15 15:09:03', '2018-01-16 15:09:03', 'active', 'yes'),
(193, 23, 'Eheliyagoda', 'eheliyagoda', 'ehel', 1, 1, '2018-01-16 15:09:03', '2018-01-17 15:09:03', 'active', 'yes'),
(194, 23, 'Kuruwita', 'kuruwita', 'kuruw', 1, 1, '2018-01-17 15:09:03', '2018-01-18 15:09:03', 'active', 'yes'),
(195, 24, 'Trincomalee', 'trincomalee', 'trin', 1, 1, '2018-01-18 15:09:03', '2018-01-19 15:09:03', 'active', 'yes'),
(196, 24, 'Kinniya', 'kinniya', 'kinn', 1, 1, '2018-01-19 15:09:03', '2018-01-20 15:09:03', 'active', 'yes'),
(197, 25, 'Vavuniya', 'vavuniya', 'vavu', 1, 1, '2018-01-20 15:09:03', '2018-01-21 15:09:03', 'active', 'yes'),
(198, 1, 'ss', NULL, NULL, NULL, NULL, '2021-05-27 05:12:59', NULL, 'active', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `location_cities_groups`
--

CREATE TABLE `location_cities_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `location_city_id` int(11) NOT NULL,
  `location_cities_group_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_cities_groups`
--

INSERT INTO `location_cities_groups` (`id`, `location_city_id`, `location_cities_group_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL, '2021-05-26 14:39:25', NULL),
(2, 2, 1, NULL, NULL, '2021-05-26 14:39:25', NULL),
(3, 3, 1, NULL, NULL, '2021-05-26 14:39:25', NULL),
(4, 4, 1, NULL, NULL, '2021-05-26 14:39:25', NULL),
(5, 5, 1, NULL, NULL, '2021-05-26 14:39:25', NULL),
(6, 6, 1, NULL, NULL, '2021-05-26 14:39:25', NULL),
(7, 7, 1, NULL, NULL, '2021-05-26 14:39:25', NULL),
(8, 8, 1, NULL, NULL, '2021-05-26 14:39:25', NULL),
(9, 9, 1, NULL, NULL, '2021-05-26 14:39:25', NULL),
(10, 10, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(11, 11, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(12, 12, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(13, 13, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(14, 14, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(15, 15, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(16, 16, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(17, 17, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(18, 18, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(19, 19, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(20, 20, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(21, 21, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(22, 22, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(23, 23, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(24, 24, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(25, 25, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(26, 26, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(27, 27, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(28, 28, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(29, 29, 1, NULL, NULL, '2021-05-26 14:39:26', NULL),
(30, 30, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(31, 31, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(32, 32, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(33, 33, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(34, 34, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(35, 35, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(36, 36, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(37, 37, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(38, 38, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(39, 39, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(40, 40, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(41, 41, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(42, 42, 1, NULL, NULL, '2021-05-26 14:39:27', NULL),
(43, 43, 3, NULL, NULL, '2021-05-26 14:39:27', NULL),
(44, 44, 3, NULL, NULL, '2021-05-26 14:39:27', NULL),
(45, 45, 3, NULL, NULL, '2021-05-26 14:39:27', NULL),
(46, 46, 3, NULL, NULL, '2021-05-26 14:39:27', NULL),
(47, 47, 3, NULL, NULL, '2021-05-26 14:39:27', NULL),
(48, 48, 3, NULL, NULL, '2021-05-26 14:39:27', NULL),
(49, 49, 3, NULL, NULL, '2021-05-26 14:39:28', NULL),
(50, 50, 3, NULL, NULL, '2021-05-26 14:39:28', NULL),
(51, 51, 3, NULL, NULL, '2021-05-26 14:39:28', NULL),
(52, 52, 3, NULL, NULL, '2021-05-26 14:39:28', NULL),
(53, 53, 3, NULL, NULL, '2021-05-26 14:39:28', NULL),
(54, 54, 3, NULL, NULL, '2021-05-26 14:39:28', NULL),
(55, 55, 3, NULL, NULL, '2021-05-26 14:39:28', NULL),
(56, 56, 3, NULL, NULL, '2021-05-26 14:39:28', NULL),
(57, 57, 3, NULL, NULL, '2021-05-26 14:39:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `location_districts`
--

CREATE TABLE `location_districts` (
  `id` int(11) UNSIGNED NOT NULL,
  `province_id` int(11) NOT NULL,
  `district_name` varchar(20) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `district_code` varchar(5) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_districts`
--

INSERT INTO `location_districts` (`id`, `province_id`, `district_name`, `slug`, `district_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 2, 'Ampara', 'all_of_ampara', 'ampa', 11, 11, '2017-08-09 17:38:18', '0000-00-00 00:00:00', 'active'),
(2, 3, 'Anuradhapura', 'all_of_anuradhapura', 'anura', 11, 11, '2017-08-09 17:38:21', '0000-00-00 00:00:00', 'active'),
(3, 8, 'Badulla', 'all_of_badulla', 'badu', 11, 11, '2017-08-09 17:38:23', '0000-00-00 00:00:00', 'active'),
(4, 2, 'Batticaloa', 'all_of_batticaloa', 'batt', 11, 11, '2017-08-09 17:38:27', '0000-00-00 00:00:00', 'active'),
(5, 9, 'Colombo', 'all_of_colombo', 'Colo', 11, 11, '2017-08-09 17:38:29', '0000-00-00 00:00:00', 'active'),
(6, 7, 'Galle', 'all_of_galle', 'Galle', 11, 11, '2017-08-09 17:38:35', '0000-00-00 00:00:00', 'active'),
(7, 9, 'Gampaha', 'all_of_gampaha', 'Gamp', 11, 11, '2017-08-09 17:38:37', '0000-00-00 00:00:00', 'active'),
(8, 7, 'Hambantota', 'all_of_hambantota', 'Hamb', 11, 11, '2017-08-09 17:38:41', '0000-00-00 00:00:00', 'active'),
(9, 4, 'Jaffna', 'all_of_jaffna', 'jaff', 11, 11, '2017-08-09 17:38:43', '0000-00-00 00:00:00', 'active'),
(10, 9, 'Kalutara', 'all_of_kalutara', 'kalu', 11, 11, '2017-08-09 17:38:44', '0000-00-00 00:00:00', 'active'),
(11, 1, 'Kandy', 'all_of_kandy', 'kand', 11, 11, '2017-08-09 17:38:46', '0000-00-00 00:00:00', 'active'),
(12, 6, 'Kegalle', 'all_of_kegalle', 'kega', 11, 11, '2017-08-09 17:38:49', '0000-00-00 00:00:00', 'active'),
(13, 4, 'Kilinochchi', 'all_of_kilinochchi', 'kili', 11, 11, '2017-08-09 17:38:56', '0000-00-00 00:00:00', 'active'),
(14, 5, 'Kurunegala', 'all_of_kurunegala', 'kuru', 11, 11, '2017-08-09 17:38:59', '0000-00-00 00:00:00', 'active'),
(15, 4, 'Mannar', 'all_of_mannar', 'mann', 11, 11, '2017-08-09 17:39:02', '0000-00-00 00:00:00', 'active'),
(16, 1, 'Matale', 'all_of_matale', 'mtle', 11, 11, '2017-08-09 17:39:04', '0000-00-00 00:00:00', 'active'),
(17, 7, 'Matara', 'all_of_matara', 'mata', 11, 11, '2017-08-09 17:39:07', '0000-00-00 00:00:00', 'active'),
(18, 8, 'Moneragala', 'all_of_moneragala', 'mona', 11, 11, '2017-08-09 17:39:09', '0000-00-00 00:00:00', 'active'),
(19, 4, 'Mullaitivu', 'all_of_mullaitivu', 'mull', 11, 11, '2017-08-09 17:39:11', '0000-00-00 00:00:00', 'active'),
(20, 1, 'Nuwara Eliya', 'all_of_nuwara_eliya', 'nuwa', 11, 11, '2017-08-09 17:39:14', '0000-00-00 00:00:00', 'active'),
(21, 3, 'Polonnaruwa', 'all_of_polonnaruwa', 'polo', 11, 11, '2017-08-09 17:39:16', '0000-00-00 00:00:00', 'active'),
(22, 5, 'Puttalam', 'all_of_puttalam', 'putt', 11, 11, '2017-08-09 17:39:18', '0000-00-00 00:00:00', 'active'),
(23, 6, 'Ratnapura', 'all_of_ratnapura', 'ratn', 11, 11, '2017-08-09 17:39:24', '0000-00-00 00:00:00', 'active'),
(24, 2, 'Trincomalee', 'all_of_trincomalee', 'trin', 11, 11, '2017-08-09 17:39:26', '0000-00-00 00:00:00', 'active'),
(25, 4, 'Vavuniya', 'all_of_vavuniya', 'vavu', 11, 11, '2017-08-09 17:39:29', '0000-00-00 00:00:00', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `location_groups`
--

CREATE TABLE `location_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `delivery_charge` float NOT NULL DEFAULT 0,
  `additional_kg` float NOT NULL DEFAULT 50,
  `currency` varchar(50) NOT NULL DEFAULT 'Rs.',
  `dilivery_days` varchar(255) NOT NULL DEFAULT '1-2',
  `delivery_days_unit` varchar(255) NOT NULL DEFAULT 'Days',
  `is_cod_available` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_groups`
--

INSERT INTO `location_groups` (`id`, `name`, `delivery_charge`, `additional_kg`, `currency`, `dilivery_days`, `delivery_days_unit`, `is_cod_available`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'CITY LIMITS', 150, 50, 'Rs.', '1-2', 'Days', 1, 0, 0, '2021-05-26 14:17:30', NULL, 'active'),
(3, 'SUBURBS', 170, 50, 'Rs.', '2-3', 'Days', 1, 0, 0, '2021-05-26 14:23:22', NULL, 'active'),
(4, 'OUTSTATION', 270, 50, 'Rs.', '3-4', 'Days', 1, 0, 0, '2021-05-26 14:23:22', NULL, 'active'),
(5, 'FAR AWAY', 380, 100, 'Rs.', '4-5', 'Days', 1, 0, 0, '2021-05-26 14:23:22', NULL, 'active'),
(6, 'REMOTE', 500, 200, 'Rs.', '4-5', 'Days', 1, 0, 0, '2021-05-26 14:23:22', NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `location_provinces`
--

CREATE TABLE `location_provinces` (
  `id` int(11) UNSIGNED NOT NULL,
  `province_name` varchar(20) NOT NULL,
  `province_code` varchar(5) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_provinces`
--

INSERT INTO `location_provinces` (`id`, `province_name`, `province_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Central', 'CP', 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active'),
(2, 'Eastern', 'EP', 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active'),
(3, 'North Central', 'NC', 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active'),
(4, 'Northern', 'NP', 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active'),
(5, 'North Western', 'NW', 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active'),
(6, 'Sabaragamuwa', 'SG', 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active'),
(7, 'Southern', 'SP', 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active'),
(8, 'Uva', 'UP', 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active'),
(9, 'Western', 'WP', 11, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `location_cities`
--
ALTER TABLE `location_cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_cities_groups`
--
ALTER TABLE `location_cities_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_districts`
--
ALTER TABLE `location_districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_groups`
--
ALTER TABLE `location_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_provinces`
--
ALTER TABLE `location_provinces`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `location_cities`
--
ALTER TABLE `location_cities`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `location_cities_groups`
--
ALTER TABLE `location_cities_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `location_districts`
--
ALTER TABLE `location_districts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `location_groups`
--
ALTER TABLE `location_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `location_provinces`
--
ALTER TABLE `location_provinces`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
